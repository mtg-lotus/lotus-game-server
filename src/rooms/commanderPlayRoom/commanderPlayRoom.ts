import { v4 } from 'uuid';

import { type Client } from 'colyseus';
import { CommanderPlayRoomState } from './commanderPlayRoom.schema';
import { MtgPlayRoom } from '../mtgPlayRoom';

export class CommanderPlayRoom extends MtgPlayRoom<CommanderPlayRoomState> {
  constructor () {
    super(CommanderPlayRoomState);
  }

  async onCreate (options: { maxPlayers?: number, name?: string, private?: boolean }): Promise<void> {
    this.roomId = v4();
    await super.onCreate(options);

    await this.setMetadata({
      format: 'commander',
    });
  }

  public async onJoin (client: Client, options: any): Promise<void> {
    await super.onJoin(client, options);
  }

  public async onLeave (client: Client, consented: boolean): Promise<void> {
    await super.onLeave(client, consented);
  }

  public async onDispose (): Promise<void> {
    await super.onDispose();
  }
}
