import { type } from '@colyseus/schema';

import { MtgPlayRoomState } from '../mtgPlayRoom';

export class CommanderPlayRoomState extends MtgPlayRoomState {
  @type({ array: 'string' }) commandZone: string[] = [];
}
