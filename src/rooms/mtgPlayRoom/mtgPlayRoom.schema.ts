import { Schema, SetSchema, type, MapSchema, ArraySchema } from '@colyseus/schema';

import { Card } from '../../cards/baseTypes';

enum Step {
  UNTAP = 'untap',
  UPKEEP = 'upkeep',
  DRAW = 'draw',
  PRE_COMBAT_MAIN = 'precombatMain',
  BEGIN_COMBAT = 'beginCombat',
  DECLARE_ATTACKERS = 'declareAttackers',
  DECLARE_BLOCKERS = 'declareBlockers',
  FIRST_COMBAT_DAMAGE = 'firstCombatDamage',
  SECOND_COMBAT_DAMAGE = 'secondCombatDamage',
  END_OF_COMBAT = 'endOfCombat',
  POST_COMBAT_MAIN = 'postCombatMain',
  END = 'end',
  CLEANUP = 'cleanup',
}

class Phase extends Schema {
  @type('string') playerId: string;

  @type('string') step: Step;
}

class ExiledCards extends Schema {
  @type('string') exiledBy: string;

  @type({ set: 'string' }) exiledCards: SetSchema<string> = new SetSchema<string>();
  @type('boolean') faceDown: boolean = false;
}

export class MtgPlayRoomState extends Schema {
  @type({ array: 'string' }) players: ArraySchema<string> = new ArraySchema<string>();

  @type({ map: Card }) cards: MapSchema<Card> = new MapSchema<Card>();

  // ZONES
  @type({ set: 'string' }) hand: SetSchema<string> = new SetSchema<string>();
  @type({ set: 'string' }) battlefield: SetSchema<string> = new SetSchema<string>();
  @type({ set: 'string' }) permanentExile: SetSchema<string> = new SetSchema<string>();
  @type({ set: 'string' }) stack: SetSchema<string> = new SetSchema<string>();
  @type({ array: ExiledCards }) exile: ArraySchema<ExiledCards> = new ArraySchema<ExiledCards>();

  @type(Phase) currentPhase: Phase;
}
