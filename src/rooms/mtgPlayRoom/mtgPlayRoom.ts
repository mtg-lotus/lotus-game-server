import { Room, type Client, ServerError } from 'colyseus';
import { type MtgPlayRoomState } from './mtgPlayRoom.schema';
import { v4 } from 'uuid';
import { EventEmitter } from 'node:events';
import { client as gqlClient } from '@utils/graphql';

import cards from '../../cards';
import { Supertype } from '../../types';
import { gql } from '@apollo/client';

export abstract class MtgPlayRoom<T extends MtgPlayRoomState> extends Room<T> {
  private readonly __StateConstructor: new () => T;

  protected _globalEventEmitter: EventEmitter;

  constructor (StateConstructor: new () => T) {
    super();
    this.__StateConstructor = StateConstructor;
  }


  public async onCreate (options: { maxPlayers?: number, name?: string, private?: boolean }): Promise<void> {
    this.setState(new this.__StateConstructor());
    this._globalEventEmitter = new EventEmitter();
    this.maxClients = options.maxPlayers ?? 2;

    if (options.name) {
      await this.setMetadata({
        name: options.name,
        private: options.private ?? false,
      });
    }

    this.onMessage('play', async (client, message: { scryfallId: string, faceIndex?: number }) => {
      const id = v4();

      const dbCard = await gqlClient.query({
        query: gql`
          query GetCardById($id: ID!) {
            getCardById(id: $id) {
              name
              oracleId
            }
          } 
        `,
        variables: {
          id: message.scryfallId
        }
      })

      if (!cards[dbCard.data.oracleId]) {
        throw new ServerError(404, `Card ${dbCard.data.name} not implemented.`);
      }

      const CardConstructor = cards[dbCard.data.oracleId][message.faceIndex ?? 0];
      const card = new CardConstructor(this._globalEventEmitter);

      card.id = id;
      card.scryfallId = message.scryfallId;
      card.oracleId = dbCard.data.oracleId;
      card.ownerId = client.sessionId;

      if (this.state.cards.has(id)) {
        throw new ServerError(500, `Card ${dbCard.data.name} has already been added`);
      }

      this.state.cards.set(id, card);

      if (card.isSupertype(Supertype.LAND)) {
        this.state.battlefield.add(id);
        this._globalEventEmitter.emit('etb', { card, state: this.state });
        card.event.emit('etb', { state: this.state });
      } else {
        this.state.stack.add(id);
        this._globalEventEmitter.emit('cast', { card, state: this.state });
        card.event.emit('cast', { state: this.state });
      }
    });
  }

  public async onJoin (client: Client, options: any): Promise<void> {
    if (this.state.players.includes(client.userData.email)) {
      console.log(client.userData.email, 'Already in the room');
      throw new ServerError(400, 'Already joined');
    } else {
      console.log(client.userData.email, 'joined!');
      this.state.players.push(client.userData.email);
    }
  }

  public async onLeave (client: Client, consented: boolean): Promise<void> {
    console.log(client.sessionId, 'left!');
  }

  public async onDispose (): Promise<void> {
    console.log('room', this.roomId, 'disposing...');
  }
}
