import { type LobbyOptions } from '@colyseus/core/build/rooms/LobbyRoom';
import { type Client, LobbyRoom, type Server } from 'colyseus';
import { IncomingMessage } from 'http';
export class GlobalLobby extends LobbyRoom {
  private readonly __server: Server;

  constructor (serverRef: Server) {
    super();
    this.__server = serverRef;
  }

  public async onCreate (options: any): Promise<void> {
    await super.onCreate(options);

    console.log('PLAYER CREATED LOBBY');
  }

  public async onJoin (client: Client, options: LobbyOptions): Promise<void> {
    super.onJoin(client, options);

    console.log('PLAYER JOINED LOBBY');
  }
}
