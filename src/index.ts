/**
 * IMPORTANT:
 * ---------
 * Do not manually edit this file if you'd like to use Colyseus Arena
 *
 * If you're self-hosting (without Arena), you can manually instantiate a
 * Colyseus Server as documented here: 👉 https://docs.colyseus.io/server/api/#constructor-options
 */
// import { listen } from "@colyseus/arena";

// // Import arena config
// import arenaConfig from "./arena.config";

// // Create and listen on 2567 (or PORT environment variable.)
// listen(arenaConfig);

import { Parser, Grammar } from 'nearley';
// import { MongoClient } from 'mongodb';
import costGrammar from '@parsing/cost';
import typeGrammar from '@parsing/type';
import oracleTextGrammar from '@parsing/oracleText';

const costParser = new Parser(Grammar.fromCompiled(costGrammar));
const typeParser = new Parser(Grammar.fromCompiled(typeGrammar));
const oracleTextParser = new Parser(Grammar.fromCompiled(oracleTextGrammar));

costParser.feed('{U}{S}{S}{P}'.toLowerCase());
typeParser.feed('Snow Host Creature Land - Human Wizard'.toLowerCase());
oracleTextParser.feed('Flying\n {R}: effect. Activate only if an opponent has 3 or more poison counters.'.toLowerCase());

console.log(costParser.results[0]);
console.log(typeParser.results[0]);

const card = oracleTextParser.results[0];

console.log(JSON.stringify(card, null, 2));

// const client = new MongoClient('mongodb://localhost:27017');

// async function main (): Promise<any> {
//   await client.connect();
//   console.log('Connected successfully to server');
//   const db = client.db('lotus');
//   const collection = db.collection('cards');

//   // the following code examples can be pasted here...

//   return await collection.insertOne(card);
// }

// void main().then(console.log).then(console.error);
// const inputStream = CharStreams.fromString(`name: "Grinning Ingus"
//   cost: {2}{R}
//   typeLine: Creature - Elemental
//   powerToughness: 2 / 2
//   oracleText: {R}, Return Grinning Ingus to its owner's hand: Add {C}{C}{R}. Activate only as a sorcery.`)
// const lexer = new CardLexer(inputStream)
// const tokenStream = new CommonTokenStream(lexer)
// const parser = new CardParser(tokenStream)

// const card = new Card(parser.card())
