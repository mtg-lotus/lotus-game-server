import { CharStreams, CommonTokenStream } from 'antlr4ts';

import { CostParser } from '@parsing/grammar/CostParser';
import { CostLexer } from '@parsing/grammar/CostLexer';

export const createCostParser = (input: string): CostParser => {
  const inputStream = CharStreams.fromString(input);

  const lexer = new CostLexer(inputStream);
  const tokenStream = new CommonTokenStream(lexer);

  return new CostParser(tokenStream);
};
