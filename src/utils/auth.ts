import fetch from 'node-fetch';

import { ServerError } from 'colyseus';
import { type VerifyRequestResponse } from '../../../server-sdk';

export const authUser = async (accessToken: string): Promise<VerifyRequestResponse['user']> => {
  const res = await fetch('http://localhost:8080/auth/verify', {
    method: 'GET',
    headers: {
      'lotus-jwt-session': accessToken,
    },
  });

  if (res.ok) {
    const { user } = await res.json() as VerifyRequestResponse;

    return user;
  }

  throw new ServerError(403, 'Unable to verify user');
};
