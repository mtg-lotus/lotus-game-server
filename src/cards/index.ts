import { Card } from './baseTypes'

import * as lands from './lands'

const cards: Record<string, (typeof Card)[]> = {
  'b2c6aa39-2d2a-459c-a555-fb48ba993373': [lands.Island]
}

export default cards