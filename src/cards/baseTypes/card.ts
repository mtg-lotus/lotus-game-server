import { EventEmitter } from 'node:events'

import { Client } from 'colyseus'
import { Schema, type, filter, SetSchema } from '@colyseus/schema'

import type { MtgPlayRoomState } from '../../rooms/mtgPlayRoom'
import { Subtype, Supertype } from '../../types'

export class Card extends Schema {
  // #region SCHEMA
  @filter(function (this: Card, client: Client, value: Card['id'], root: MtgPlayRoomState): boolean {
    if (this.ownerId === client.sessionId && this.revealedTo.has(client.sessionId)) {
      return true // Should always see your own cards and cards revealed to you
    }

    if (root.hand.has(this.id)) {
      return false
    }

    const exiled = root.exile.find((e) => e.exiledCards.has(this.id))
    if (exiled && exiled.faceDown) {
      return false
    }

    return true
  })
  @type('string') id: string

  @type('string') ownerId: string

  // EXTERNAL IDS
  @type('string') oracleId: string
  @type('string') scryfallId: string

  @type({ set: 'string' }) revealedTo: SetSchema<string> = new SetSchema<string>()
  @type('string') type: 'spell' | 'permanent'

  // #endregion

  protected _globalEventEmitter: EventEmitter
  protected _name: string
  protected _supertypes: Supertype[] = []
  protected _subtypes: Subtype[] = []

  protected readonly _localEventEmitter: EventEmitter

  public get name(): string {
    return this._name
  }

  public get event(): EventEmitter {
    return this._localEventEmitter
  }

  constructor(globalEventEmitter: EventEmitter) {
    super()
    this._globalEventEmitter = globalEventEmitter
    this._localEventEmitter = new EventEmitter()

    this._globalEventEmitter.on('etb', this._onCardEntersBattlefield.bind(this))
    this._globalEventEmitter.on('cast', this._onCardCast.bind(this))

    this._localEventEmitter.on('etb', this._onThisEntersBattlefield.bind(this))
    this._localEventEmitter.on('cast', this._onThisCast.bind(this))
  }

  public isSupertype(type: Supertype): boolean {
    return this._supertypes.includes(type)
  }

  public isSubtype(type: Subtype): boolean {
    return this._subtypes.includes(type)
  }

  protected _onThisEntersBattlefield(_args: { state: MtgPlayRoomState }): void { }

  protected _onThisCast(_args: { state: MtgPlayRoomState }): void { }

  protected _onCardEntersBattlefield(_args: { state: MtgPlayRoomState, card: Card }): void { }

  protected _onCardCast(_args: { state: MtgPlayRoomState, card: Card }): void { }
}

