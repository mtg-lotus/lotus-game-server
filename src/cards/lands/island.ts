import { type EventEmitter } from 'node:events';

import { type Card, Land } from '../baseTypes';

import { Subtype, Supertype } from '../../types';
import { type MtgPlayRoomState } from '../../rooms/mtgPlayRoom';

class Island extends Land {
  constructor (globalEventEmitter: EventEmitter) {
    super(globalEventEmitter);

    this._name = 'Island';
    this._supertypes.push(Supertype.LAND);
    this._subtypes.push(Subtype.ISLAND);
  }

  protected _onCardEntersBattlefield (args: { state: MtgPlayRoomState, card: Card }): void {
    console.log('A CARD ETBd', args.card, args.state);
  }

  protected _onThisEntersBattlefield (args: { state: MtgPlayRoomState }): void {
    console.log('THIS ETBd', this, args.state);
  }
}

export default Island;
