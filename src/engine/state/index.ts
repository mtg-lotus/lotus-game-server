import { Schema, type, ArraySchema } from '@colyseus/schema';

export type StateKey = keyof GameState;

export class GameState extends Schema {
  @type({ array: 'string' }) players: ArraySchema<string> = new ArraySchema<string>();
}
