export type MetadataKey = keyof GameMetadata;

export class GameMetadata {
  public startingLifeTotal: number = 20;
}
