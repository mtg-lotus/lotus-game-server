import { type MetadataKey } from 'src/engine/metadata';
import { type StateKey } from 'src/engine/state';
import { type Number } from '../common';
import { type Counter, type Keyword, type Player, type PlayerDesignation } from '../enums';
import { type GameObject } from '../gameObject';
import { type ZoneChange } from '../zoneChange';
import { Condition, ConditionSubType, ConditionType } from './condition';

export enum StaticPlayerConditionQualifierType {
  WITH_DIFFERENT_POWERS = 'WITH_DIFFERENT_POWERS',
  WITH_POWER = 'WITH_POWER',
  FOUGHT_THIS_TURN = 'FOUGHT_THIS_TURN',
  WITH_KEYWORD = 'WITH_KEYWORD',
}

export interface StaticPlayerConditionQualifier {
  type: StaticPlayerConditionQualifierType
  number?: Number
  keyword?: Keyword
}

export abstract class StaticPlayerCondition extends Condition {
  public readonly type: ConditionType.STATIC_PLAYER_CONDITION = ConditionType.STATIC_PLAYER_CONDITION;

  public player?: Player;

  public addPlayer (player: Player): void {
    this.player = player;
  }
}

export class ControlObjectStaticPlayerCondition extends StaticPlayerCondition {
  public readonly subType: ConditionSubType.CONTROLS_OBJECTS = ConditionSubType.CONTROLS_OBJECTS;

  public readonly number: Number;
  public readonly gameObject: GameObject;
  public readonly qualifier?: StaticPlayerConditionQualifier;

  constructor (number: Number, gameObject: GameObject, qualifier?: StaticPlayerConditionQualifier) {
    super();
    this.number = number;
    this.gameObject = gameObject;
    this.qualifier = qualifier;
  }
}

export class GainedLifeStaticPlayerCondition extends StaticPlayerCondition {
  public readonly subType: ConditionSubType.GAINED_LIFE = ConditionSubType.GAINED_LIFE;
}

export class HadZoneChangeStaticPlayerCondition extends StaticPlayerCondition {
  public readonly subType: ConditionSubType.HAD_ZONE_CHANGE = ConditionSubType.HAD_ZONE_CHANGE;

  public readonly number: Number;
  public readonly gameObject: GameObject;
  public readonly zoneChange: ZoneChange;

  constructor (number: Number, gameObject: GameObject, zoneChange: ZoneChange) {
    super();
    this.number = number;
    this.gameObject = gameObject;
    this.zoneChange = zoneChange;
  }
}

export class HasAttackedStaticPlayerCondition extends StaticPlayerCondition {
  public readonly subType: ConditionSubType.HAS_ATTACKED = ConditionSubType.HAS_ATTACKED;

  // This should only be "this turn" can add more if not
}

export class HasCountersStaticPlayerCondition extends StaticPlayerCondition {
  public readonly subType: ConditionSubType.HAS_COUNTERS = ConditionSubType.HAS_COUNTERS;

  public readonly number: Number;
  public readonly counter: Counter;

  constructor (number: Number, counter: Counter) {
    super();
    this.number = number;
    this.counter = counter;
  }
}

export class HasDesignationStaticPlayerCondition extends StaticPlayerCondition {
  public readonly subType: ConditionSubType.HAS_DESIGNATION = ConditionSubType.HAS_DESIGNATION;

  public readonly designation: PlayerDesignation;

  constructor (designation: PlayerDesignation) {
    super();
    this.designation = designation;
  }
}

export class HasLifeStaticPlayerCondition extends StaticPlayerCondition {
  public readonly subType: ConditionSubType.HAS_LIFE = ConditionSubType.HAS_LIFE;

  public readonly number: Number;
  public readonly compareAgainst: StateKey | MetadataKey; // Idk how general this needs to be yet

  constructor (number: Number, compareAgainst: StateKey | MetadataKey) {
    super();
    this.number = number;
    this.compareAgainst = compareAgainst;
  }
}
