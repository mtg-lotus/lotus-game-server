import { type Number } from '../common';
import { type Player, type Zone } from '../enums';
import { type GameObject } from '../gameObject';
import { Condition, ConditionSubType, ConditionType } from './condition';

export enum StaticObjectConditionQualifierType {
  IN_ZONE = 'IN_ZONE',
  ABOVE_THIS = 'ABOVE_THIS',
}

export interface StaticObjectConditionQualifier {
  type: StaticObjectConditionQualifierType
  player?: Player
  zone?: Zone
}

export abstract class StaticObjectCondition extends Condition {
  public readonly type: ConditionType.STATIC_OBJECT_CONDITION = ConditionType.STATIC_OBJECT_CONDITION;

  public gameObject?: GameObject;

  public addObject (gameObject: GameObject): void {
    this.gameObject = gameObject;
  }
}

export class CountObjectsStaticObjectCondition extends StaticObjectCondition {
  public readonly subType: ConditionSubType.COUNT_OBJECTS = ConditionSubType.COUNT_OBJECTS;

  public readonly qualifier: StaticObjectConditionQualifier;

  constructor (qualifier: StaticObjectConditionQualifier) {
    super();
    this.qualifier = qualifier;
  }
}

export class DiedThisTurnStaticObjectCondition extends StaticObjectCondition {
  public readonly subType: ConditionSubType.DIED_THIS_TURN = ConditionSubType.DIED_THIS_TURN;
}

export class HasTotalPowerStaticObjectCondition extends StaticObjectCondition {
  public readonly subType: ConditionSubType.HAS_TOTAL_POWER = ConditionSubType.HAS_TOTAL_POWER;

  public readonly number: Number;

  constructor (number: Number) {
    super();
    this.number = number;
  }
}

export class IsAttackingStaticObjectCondition extends StaticObjectCondition {
  public readonly subType: ConditionSubType.IS_ATTACKING = ConditionSubType.IS_ATTACKING;
}
