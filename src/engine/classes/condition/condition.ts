export enum ConditionType {
  STATIC_PLAYER_CONDITION = 'STATIC_PLAYER_CONDITION',
  STATIC_OBJECT_CONDITION = 'STATIC_OBJECT_CONDITION',
}

export enum ConditionSubType {
  // #region STATIC_PLAYER_CONDITION

  CONTROLS_OBJECTS = 'CONTROLS_OBJECTS',
  GAINED_LIFE = 'GAINED_LIFE',
  HAD_ZONE_CHANGE = 'HAD_ZONE_CHANGE',
  HAS_ATTACKED = 'HAS_ATTACKED',
  HAS_COUNTERS = 'HAS_COUNTERS',
  HAS_DESIGNATION = 'HAS_DESIGNATION',
  HAS_LIFE = 'HAS_LIFE',

  // #endregion
  // #region STATIC_OBJECT_CONDITION

  COUNT_OBJECTS = 'COUNT_OBJECTS',
  HAS_TOTAL_POWER = 'HAS_TOTAL_POWER',
  IS_ATTACKING = 'IS_ATTACKING',
  DIED_THIS_TURN = 'DIED_THIS_TURN',

  // #endregion
}

export abstract class Condition {
  public abstract readonly type: ConditionType;
  public abstract readonly subType: ConditionSubType;
}
