import { type Paragraph } from '../paragraph';

export class Card {
  public rules: Paragraph[] = [];

  constructor (pgList: Paragraph[]) {
    this.rules = pgList;
  }
}
