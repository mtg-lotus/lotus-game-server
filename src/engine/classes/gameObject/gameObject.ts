import { type Player, type CardType, type SubType, type SuperType } from '../enums';
import { type Number } from '../common';

export enum ObjectType {
  CARD = 'CARD',
  PERMANENT = 'PERMANENT',
  PLAYER = 'PLAYER',
  SPELL = 'SPELL',
  THIS = 'THIS',
}

export enum GameObjectQualifierType {
  CONTROLLED_BY = 'CONTROLLED_BY',
}

export interface GameObjectQualifier {
  type: GameObjectQualifierType
  player?: Player
}

export abstract class GameObject {
  public abstract readonly type: ObjectType;

  public number?: Number;
  public qualifier?: GameObjectQualifier;

  public addNumber (number: Number): void {
    this.number = number;
  }

  public addQualifier (qualifier: GameObjectQualifier): void {
    this.qualifier = qualifier;
  }
}

export class CardGameObject extends GameObject {
  public readonly type: ObjectType.CARD = ObjectType.CARD;

  public readonly superTypes?: SuperType[];
  public readonly cardTypes?: CardType[];
  public readonly subTypes?: SubType[];

  constructor (superTypes?: SuperType[], cardTypes?: CardType[], subTypes?: SubType[]) {
    super();
    this.superTypes = superTypes;
    this.cardTypes = cardTypes;
    this.subTypes = subTypes;
  }
}

export class PermanentGameObject extends GameObject {
  public readonly type: ObjectType.PERMANENT = ObjectType.PERMANENT;

  public readonly superTypes?: SuperType[];
  public readonly cardTypes?: CardType[];
  public readonly subTypes?: SubType[];

  constructor (superTypes?: SuperType[], cardTypes?: CardType[], subTypes?: SubType[]) {
    super();
    this.superTypes = superTypes;
    this.cardTypes = cardTypes;
    this.subTypes = subTypes;
  }
}

export class PlayerGameObject extends GameObject {
  public readonly type: ObjectType.PLAYER = ObjectType.PLAYER;
}

export class SpellGameObject extends GameObject {
  public readonly type: ObjectType.SPELL = ObjectType.SPELL;

  public readonly cardTypes?: CardType[];
  public readonly subTypes?: SubType[];

  constructor (cardTypes?: CardType[], subTypes?: SubType[]) {
    super();
    this.cardTypes = cardTypes;
    this.subTypes = subTypes;
  }
}

export class ThisGameObject extends GameObject {
  public readonly type: ObjectType.THIS = ObjectType.THIS;
}
