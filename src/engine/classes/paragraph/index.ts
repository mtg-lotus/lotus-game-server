import { type Ability } from '../ability';
import { type Keyword } from '../enums';

export abstract class Paragraph {
  public abstract type: 'ABILITY' | 'KEYWORD';
  public abstract value: Ability | Keyword[];

  public helpText?: string;

  constructor (helpText?: string) {
    this.helpText = helpText;
  }
}

export class AbilityParagraph extends Paragraph {
  public type: 'ABILITY' = 'ABILITY';
  public value: Ability;

  constructor (ability: Ability, helpText?: string) {
    super(helpText);
    this.value = ability;
  }
}

export class KeywordParagraph extends Paragraph {
  public type: 'KEYWORD' = 'KEYWORD';
  public value: Keyword[];

  constructor (keywords: Keyword[], helpText?: string) {
    super(helpText);
    this.value = keywords;
  }
}
