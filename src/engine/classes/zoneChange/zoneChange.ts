import { type Player } from '../enums';
import { type Zone } from '../enums/zone';

export enum ZoneChangeType {
  ENTER = 'ENTER',
  LEAVE = 'LEAVE',
}

export enum ZoneChangeQualifierType {
  THIS_TURN = 'THIS_TURN',
  UNDER_PLAYERS_CONTROL = 'UNDER_PLAYERS_CONTROL',
}

export interface ZoneChangeQualifier {
  type: ZoneChangeQualifierType
  subject?: Player
}

export abstract class ZoneChange {
  public abstract readonly type: ZoneChangeType;

  public readonly zone: Zone;
  public readonly subject: Player;
  public readonly qualifiers: ZoneChangeQualifier[] = [];

  constructor (zone: Zone, subject: Player) {
    this.zone = zone;
    this.subject = subject;
  }

  public addQualifier (qualifier: ZoneChangeQualifier): void {
    this.qualifiers.push(qualifier);
  }
}

export class EnterZoneChange extends ZoneChange {
  public readonly type: ZoneChangeType.ENTER = ZoneChangeType.ENTER;
}

export class LeaveZoneChange extends ZoneChange {
  public readonly type: ZoneChangeType.LEAVE = ZoneChangeType.LEAVE;
}
