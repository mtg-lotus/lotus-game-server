import { type Condition } from 'src/engine/classes/condition';
import { type Player } from 'src/engine/classes/enums/player';
import { type PriorityType } from 'src/engine/classes/enums/priority';
import { type Phase, type Step, type StepPhaseQualifier } from 'src/engine/classes/enums/stepPhase';

export enum ActivatedAbilityRestrictionType {
  PRIORITY = 'PRIORITY',
  STEP = 'STEP',
  PHASE = 'PHASE',
  TURN = 'TURN',
  CONDITION = 'CONDITION',
}

export abstract class ActivatedAbilityRestriction {
  public abstract readonly type: ActivatedAbilityRestrictionType;
}

export class PriorityBasedActivatedAbilityRestriction extends ActivatedAbilityRestriction {
  public readonly type: ActivatedAbilityRestrictionType.PRIORITY = ActivatedAbilityRestrictionType.PRIORITY;

  public readonly priorityType: PriorityType;

  constructor (priorityType: PriorityType) {
    super();
    this.priorityType = priorityType;
  }
}

export class StepBasedActivatedAbilityResctriction extends ActivatedAbilityRestriction {
  public readonly type: ActivatedAbilityRestrictionType.STEP = ActivatedAbilityRestrictionType.STEP;

  public readonly qualifier: StepPhaseQualifier;
  public readonly step: Step;
  public readonly player?: Player;

  constructor (qualifier: StepPhaseQualifier, step: Step, player?: Player) {
    super();
    this.qualifier = qualifier;
    this.step = step;
    this.player = player;
  }
}

export class PhaseBasedActivatedAbilityRestriction extends ActivatedAbilityRestriction {
  public readonly type: ActivatedAbilityRestrictionType.PHASE = ActivatedAbilityRestrictionType.PHASE;

  public readonly qualifier: StepPhaseQualifier;
  public readonly phase: Phase;

  constructor (qualifier: StepPhaseQualifier, phase: Phase) {
    super();
    this.qualifier = qualifier;
    this.phase = phase;
  }
}

export class TurnBasedActivatedAbilityRestriction extends ActivatedAbilityRestriction {
  public readonly type: ActivatedAbilityRestrictionType.TURN = ActivatedAbilityRestrictionType.TURN;

  public readonly qualifier: StepPhaseQualifier;
  public readonly player?: Player;

  constructor (qualifier: StepPhaseQualifier, player?: Player) {
    super();
    this.qualifier = qualifier;
    this.player = player;
  }
}

export class ConditionBasedActivatedAbilityRestriction extends ActivatedAbilityRestriction {
  public readonly type: ActivatedAbilityRestrictionType.CONDITION = ActivatedAbilityRestrictionType.CONDITION;

  public readonly condition: Condition;

  constructor (condition: Condition) {
    super();
    this.condition = condition;
  }
}
