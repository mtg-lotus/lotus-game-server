import { Ability, AbilityType } from '../ability';
import { type Cost } from '../../cost';
import { type Effect } from '../../effect';
import { type ActivatedAbilityRestriction } from './activatedAbilityResctriction';

export class ActivatedAbility extends Ability {
  public readonly type: AbilityType.ACTIVATED = AbilityType.ACTIVATED;
  public cost: Cost[];
  public restrictions?: ActivatedAbilityRestriction[];

  constructor (cost: Cost[], effects: Effect[], restrictions?: ActivatedAbilityRestriction[]) {
    super(effects);
    this.cost = cost;
    this.restrictions = restrictions;
  }
}
