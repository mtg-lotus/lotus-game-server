import { Ability, AbilityType } from './ability';

export class TriggeredAbility extends Ability {
  public readonly type: AbilityType.TRIGGERED = AbilityType.TRIGGERED;
}
