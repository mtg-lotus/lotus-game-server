import { type Effect } from '../effect';

export enum AbilityType {
  ACTIVATED = 'ACTIVATED',
  ONE_SHOT = 'ONE_SHOT',
  STATIC = 'STATIC',
  TRIGGERED = 'TRIGGERED',
}

export abstract class Ability {
  public abstract readonly type: AbilityType;
  public effects: Effect[];

  constructor (effects: Effect[]) {
    this.effects = effects;
  }
}
