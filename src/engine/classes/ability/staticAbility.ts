import { Ability, AbilityType } from './ability';

export class StaticAbility extends Ability {
  public readonly type: AbilityType.STATIC = AbilityType.STATIC;
}
