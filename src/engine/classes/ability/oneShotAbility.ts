import { Ability, AbilityType } from './ability';

export class OneShotAbility extends Ability {
  public readonly type: AbilityType.ONE_SHOT = AbilityType.ONE_SHOT;
}
