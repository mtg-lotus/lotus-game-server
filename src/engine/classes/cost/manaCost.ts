import { Cost, CostType } from './cost';
import { type ManaCost as ManaCostEnum } from '../enums';

export class ManaCost extends Cost {
  public readonly type: CostType.MANA = CostType.MANA;

  public readonly cost: ManaCostEnum[];

  constructor (cost: ManaCostEnum[]) {
    super();
    this.cost = cost;
  }
}
