import { Cost, CostType } from './cost';

export enum TapUntapCost {
  TAP = 'TAP',
  UNTAP = 'UNTAP',
}

export class TapUntap extends Cost {
  public type: CostType.TAP_UNTAP = CostType.TAP_UNTAP;

  public readonly value: TapUntapCost;

  constructor (value: TapUntapCost) {
    super();
    this.value = value;
  }
}
