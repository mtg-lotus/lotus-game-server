export enum CostType {
  MANA = 'MANA',
  PLAYER_ACTION = 'PLAYER_ACTION',
  TAP_UNTAP = 'TAP_UNTAP',
}

export enum PlayerActionType {
  SACRIFICE = 'SACRIFICE',
  PAY_LIFE = 'PAY_LIFE',
  REMOVE_COUNTERS = 'REMOVE_COUNTERS',
}

export abstract class Cost {
  public abstract readonly type: CostType;
}
