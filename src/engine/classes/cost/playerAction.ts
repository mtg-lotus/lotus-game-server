import { type GameObject } from '../gameObject';
import { Cost, CostType, PlayerActionType } from './cost';
import { type Number } from '../common';

export abstract class PlayerAction extends Cost {
  public readonly type: CostType.PLAYER_ACTION = CostType.PLAYER_ACTION;
  public abstract readonly subType: PlayerActionType;
}

export class SacrificePlayerAction extends PlayerAction {
  public readonly subType: PlayerActionType.SACRIFICE = PlayerActionType.SACRIFICE;

  public readonly gameObject: GameObject;
  public readonly number?: Number;

  constructor (gameObject: GameObject, number?: Number) {
    super();
    this.number = number;
    this.gameObject = gameObject;
  }
}

export class PayLifePlayerAction extends PlayerAction {
  public readonly subType: PlayerActionType.PAY_LIFE = PlayerActionType.PAY_LIFE;

  public readonly number: Number;

  constructor (number: Number) {
    super();
    this.number = number;
  }
}

export class RemoveCountersPlayerAction extends PlayerAction {
  public readonly subType: PlayerActionType.REMOVE_COUNTERS = PlayerActionType.REMOVE_COUNTERS;

  public readonly number: Number;
  public readonly gameObject: GameObject;

  constructor (number: Number, gameObject: GameObject) {
    super();
    this.number = number;
    this.gameObject = gameObject;
  }
}
