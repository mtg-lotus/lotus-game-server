export enum Comparrison {
  GTE = 'GTE',
  LTE = 'LTE',
  EQ = 'EQ',
  NEQ = 'NEQ',
}

export class Number {
  public readonly value: number;
  public readonly comparrison: Comparrison;

  constructor (value: number, comparrison?: Comparrison) {
    this.value = value;
    this.comparrison = comparrison ?? Comparrison.EQ;
  }

  public compareAgainst (value: number): boolean {
    switch (this.comparrison) {
      case Comparrison.GTE: {
        return value >= this.value;
      }
      case Comparrison.LTE: {
        return value <= this.value;
      }
      case Comparrison.EQ: {
        return value === this.value;
      }
      case Comparrison.NEQ: {
        return value !== this.value;
      }
    }
  }
}
