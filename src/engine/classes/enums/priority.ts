export enum PriorityType {
  NONE = 'NONE', // lands, split-second, mana ability, etc
  SORCERY_SPEED = 'SORCERY_SPEED',
  INSTANT_SPEED = 'INSTANT_SPEED',
}
