export enum StepPhaseQualifier {
  BEFORE = 'BEFORE',
  DURING = 'DURING',
  AFTER = 'AFTER',
  EACH = 'EACH', // Theoretically this may not be mutually exclusive from the others? (eg. after each)
}

export enum Phase {
  BEGINNING = 'BEGINNING',
  PRECOMBAT_MAIN = 'PRECOMBAT_MAIN',
  COMBAT = 'COMBAT',
  POSTCOMBAT_MAIN = 'POSTCOMBAT_MAIN',
  ENDING = 'ENDING',
}

export enum BeginningStep {
  UNTAP = 'UNTAP',
  UPKEEP = 'UPKEEP',
  DRAW = 'DRAW',
}

export enum CombatStep {
  BEGIN_COMBAT = 'BEGIN_COMBAT',
  DECLARE_ATTACKERS = 'DECLARE_ATTACKERS',
  DECLARE_BLOCKERS = 'DECLARE_BLOCKERS',
  COMBAT_DAMAGE = 'COMBAT_DAMAGE',
  END_COMBAT = 'END_COMBAT',
}

export enum EndingStep {
  END_STEP = 'END_STEP',
  CLEANUP = 'CLEANUP',
}

export type Step = BeginningStep | CombatStep | EndingStep;
