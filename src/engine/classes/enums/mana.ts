export enum BasicMana {
  __ERROR = '__ERROR',
  COLORLESS = 'COLORLESS',
  WHITE = 'WHITE',
  BLUE = 'BLUE',
  BLACK = 'BLACK',
  RED = 'RED',
  GREEN = 'GREEN',
}

export enum ManaCost {
  __ERROR = '__ERROR',
  X_GENERIC = 'X_GENERIC',
  Y_GENERIC = 'Y_GENERIC',
  Z_GENERIC = 'Z_GENERIC',
  ZERO_GENERIC = 'ZERO_GENERIC',
  HALF_GENERIC = 'HALF_GENERIC',
  ONE_GENERIC = 'ONE_GENERIC',
  TWO_GENERIC = 'TWO_GENERIC',
  THREE_GENERIC = 'THREE_GENERIC',
  FOUR_GENERIC = 'FOUR_GENERIC',
  FIVE_GENERIC = 'FIVE_GENERIC',
  SIX_GENERIC = 'SIX_GENERIC',
  SEVEN_GENERIC = 'SEVEN_GENERIC',
  EIGHT_GENERIC = 'EIGHT_GENERIC',
  NINE_GENERIC = 'NINE_GENERIC',
  TEN_GENERIC = 'TEN_GENERIC',
  ELEVEN_GENERIC = 'ELEVEN_GENERIC',
  TWELVE_GENERIC = 'TWELVE_GENERIC',
  THIRTEEN_GENERIC = 'THIRTEEN_GENERIC',
  FOURTEEN_GENERIC = 'FOURTEEN_GENERIC',
  FIFTEEN_GENERIC = 'FIFTEEN_GENERIC',
  SIXTEEN_GENERIC = 'SIXTEEN_GENERIC',
  SEVENTEEN_GENERIC = 'SEVENTEEN_GENERIC',
  EIGHTEEN_GENERIC = 'EIGHTEEN_GENERIC',
  NINETEEN_GENERIC = 'NINETEEN_GENERIC',
  TWENTY_GENERIC = 'TWENTY_GENERIC',
  WHITE_BLUE = 'WHITE_BLUE',
  WHITE_BLACK = 'WHITE_BLACK',
  BLACK_RED = 'BLACK_RED',
  BLACK_GREEN = 'BLACK_GREEN',
  BLUE_BLACK = 'BLUE_BLACK',
  BLUE_RED = 'BLUE_RED',
  RED_GREEN = 'RED_GREEN',
  RED_WHITE = 'RED_WHITE',
  GREEN_WHITE = 'GREEN_WHITE',
  GREEN_BLUE = 'GREEN_BLUE',
  BLACK_GREEN_PHY = 'BLACK_GREEN_PHY',
  BLACK_RED_PHY = 'BLACK_RED_PHY',
  GREEN_BLUE_PHY = 'GREEN_BLUE_PHY',
  GREEN_WHITE_PHY = 'GREEN_WHITE_PHY',
  RED_GREEN_PHY = 'RED_GREEN_PHY',
  RED_WHITE_PHY = 'RED_WHITE_PHY',
  BLUE_BLACK_PHY = 'BLUE_BLACK_PHY',
  BLUE_RED_PHY = 'BLUE_RED_PHY',
  WHITE_BLACK_PHY = 'WHITE_BLACK_PHY',
  WHITE_BLUE_PHY = 'WHITE_BLUE_PHY',
  PHYREXIAN_GENERIC = 'PHYREXIAN_GENERIC',
  WHITE_PHY = 'WHITE_PHY',
  BLUE_PHY = 'BLUE_PHY',
  BLACK_PHY = 'BLACK_PHY',
  RED_PHY = 'RED_PHY',
  GREEN_PHY = 'GREEN_PHY',
  HUNDRED_GENERIC = 'HUNDRED_GENERIC',
  MILLION_GENERIC = 'MILLION_GENERIC',
  INFINITY_GENERIC = 'INFINITY_GENERIC',
  HALF_WHITE = 'HALF_WHITE',
  HALF_RED = 'HALF_RED',
  SNOW_MANA = 'SNOW_MANA',
}
