export enum EffectType {
  EFFECT = 'EFFECT',
}

export class Effect {
  public type: EffectType;

  constructor (type: EffectType) {
    this.type = type;
  }
}
