@include "./Enums.ne"

manaCost ->
  (singleManaCost _):+ {% ([d]): ManaCost[] =>  d.map(([cost]) => cost) %}