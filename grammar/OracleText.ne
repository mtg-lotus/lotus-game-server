@include "./Enums.ne"

@{%
import {
  ActivatedAbility,
  StaticAbility,
  OneShotAbility,
  TriggeredAbility,
  ActivatedAbilityRestriction,
  PriorityBasedActivatedAbilityRestriction,
  StepBasedActivatedAbilityResctriction,
  TurnBasedActivatedAbilityRestriction,
  ConditionBasedActivatedAbilityRestriction,
} from '@engine/classes/ability';

import {
  Card
} from '@engine/classes/card';

import {
  Number,
  Comparrison,
} from '@engine/classes/common';

import {
  ControlObjectStaticPlayerCondition,
  GainedLifeStaticPlayerCondition,
  HadZoneChangeStaticPlayerCondition,
  HasAttackedStaticPlayerCondition,
  HasCountersStaticPlayerCondition,
  HasDesignationStaticPlayerCondition,
  HasLifeStaticPlayerCondition,
  StaticPlayerConditionQualifierType,
  StaticPlayerConditionQualifier,
  CountObjectsStaticObjectCondition,
  DiedThisTurnStaticObjectCondition,
  HasTotalPowerStaticObjectCondition,
  IsAttackingStaticObjectCondition,
  StaticObjectConditionQualifierType,
  StaticObjectConditionQualifier,
} from '@engine/classes/condition';

import {
  Cost,
  ManaCost,
  PlayerAction,
  SacrificePlayerAction,
  PayLifePlayerAction,
  RemoveCountersPlayerAction,
  TapUntap,
} from '@engine/classes/cost';

import {
  Effect,
  EffectType,
} from '@engine/classes/effect';

import {
  BeginningStep,
  CombatStep,
  Counter,
  Player,
  PlayerDesignation,
  PriorityType,
  StepPhaseQualifier,
  Zone,
} from '@engine/classes/enums';

import {
  CardGameObject,
  GameObjectQualifier,
  GameObjectQualifierType,
  ObjectType,
  PermanentGameObject,
  PlayerGameObject,
  SpellGameObject,
  ThisGameObject,
} from '@engine/classes/gameObject';

import {
  Paragraph,
  AbilityParagraph,
  KeywordParagraph,
} from '@engine/classes/paragraph';

import {
  EnterZoneChange,
  LeaveZoneChange,
  ZoneChange,
  ZoneChangeQualifier,
  ZoneChangeQualifierType,
} from '@engine/classes/zoneChange';

const extractFromList = <T>(list: T[][] | null | undefined, index: number): T[] => {
  if (!list) {
    return []
  }

  return list.map((item) => item[index])
}

const extractFromOptional = <T>(item: T[] | null | undefined, index: number): T | null => {
  if (!item) {
    return null
  }

  return item[index]
}

%}

# region CARD

card ->
  %nl:* paragraphWithHelpText (%nl:+ paragraphWithHelpText):* %nl:* {%
    ([, pg, pgList]): Card => {
      const paragraphList: Paragraph[] = [
        pg,
        ...extractFromList(pgList, 1)
      ]

      return new Card(paragraphList);
    }
  %}
  | _ {%
    () => new Card([])
  %}

# endregion

# region PARAGRAPH

paragraphWithHelpText ->
  _ ability _ %helpText:? {%
    ([, ability, , helpText]): Paragraph => {
      return new AbilityParagraph(
        ability,
        helpText?.text ?? undefined,
      );
    }
  %}
  | _ keywords _ %helpText:? {%
    ([, keywords, , helpText]): Paragraph => {
      return new KeywordParagraph(
        keywords,
        helpText?.text ?? undefined,
      );
    }
  %}

# endregion

# region ABILITY

ability ->
  activatedAbility {% id %}
  | triggeredAbility {% id %}
  | staticAbility {% id %}
  | oneShotAbility {% id %}

activatedAbility ->
  costs %colon __ effect (__ activationResctriction):? {%
    ([costs, , , effect, qualifier]) => {
      return new ActivatedAbility(
        costs,
        [effect],
        extractFromOptional(qualifier, 1) ?? undefined,
      )
    }
  %}

triggeredAbility ->
  "whenever" effect {%
    ([condition, effect]) => {
      return new TriggeredAbility([effect]);
    }
  %}

staticAbility ->
  effect {%
    ([effect]) => {
      return new StaticAbility([effect]);
    }
  %}

oneShotAbility ->
  effect {%
    ([effect]) => {
      return new OneShotAbility([effect]);
    }
  %}

# endregion

# region GAME_OBJECT

objectType ->
  ("card" | "cards") {% () => ObjectType.CARD %}
  | ("permanent" | "permanents") {% () => ObjectType.PERMANENT %}
  | ("player" | "players") {% () => ObjectType.PLAYER %}
  | ("spell" | "spells") {% () => ObjectType.SPELL %}

object ->
  ("{this}" | "{this}'s") {%
    () => {
      return new ThisGameObject();
    }
  %}
  | (superType __):* cardType objectType:? {%
    ([superTypeList, cardType, objectType]) => {
      console.log('THIS ONE', superTypeList, cardType, objectType)
      switch (objectType) {
        case undefined:
        case null:
        case ObjectType.CARD: {
          return new CardGameObject(
            extractFromList(superTypeList, 0),
            [cardType]
          );
        }
        case ObjectType.PERMANENT: {
          return new PermanentGameObject(
            extractFromList(superTypeList, 0),
            [cardType]
          )
        }
        case ObjectType.SPELL: {
          return new SpellGameObject(
            [cardType]
          )
        }
      }
    }
  %}
  | subType (__ cardType):? {%
    ([subType, cardType]) => new CardGameObject(
      undefined,
      cardType ? [cardType[1]] : undefined,
      [subType]
    )
  %}
  | objectType {%
    ([objectType]) => {
      switch (objectType) {
        case ObjectType.CARD: {
          return new CardGameObject();
        }
        case ObjectType.PERMANENT: {
          return new PermanentGameObject();
        }
        case ObjectType.PLAYER: {
          return new PlayerGameObject();
        }
        case ObjectType.SPELL: {
          return new SpellGameObject();
        }
      }
    }
  %}

qualifiedObject ->
  (number __):? object (__ objectQualifier):? {%
    ([num, object, qualifier]) => {
      if (num) {
        object.addNumber(num[0]);
      }
      if (qualifier) {
        object.addQualifier(qualifier[1]);
      }

      return object;
    }
  %}

objectQualifier ->
  player __ ("control" | "controls") {%
    ([player]): GameObjectQualifier => ({
      type: GameObjectQualifierType.CONTROLLED_BY,
      player,
    })
  %}

objectVerb ->
  "is"
  | "are"
  | "have"
  | "has"
  | "had"

#endregion

# region CONDITION

condition ->
  player __ (objectVerb __):? staticPlayerCondition {%
    ([player, , , staticPlayerCondition]) => {
      staticPlayerCondition.addPlayer(player);

      return staticPlayerCondition;
    }
  %}
  | qualifiedObject __ (objectVerb __):? staticObjectCondition {%
    ([object, , , staticObjectCondition]) => {
      console.log('OBJ', object, staticObjectCondition.prototype);
      staticObjectCondition.addObject(object);

      return staticObjectCondition;
    }
  %}

staticPlayerCondition ->
  controlObject {% id %}
  | gainedLife {% id %}
  | hadZoneChange {% id %}
  | hasAttacked {% id %}
  | hasCounters {% id %}
  | hasDesignation {% id %}
  | hasLife {% id %}

staticObjectCondition ->
  countObjects {% id %}
  | diedThisTurn {% id %}
  | hasTotalPower {% id %}
  | isAttacking {% id %}

controlObject ->
  ("control" | "controls") __ number __ object (__ additionalPermanentQualifier):? {%
    ([, , number, , object, qualifier]): ControlObjectStaticPlayerCondition => {
      const qual = extractFromOptional<StaticPlayerConditionQualifier>(qualifier, 1) ?? undefined;

      return new ControlObjectStaticPlayerCondition(
        number,
        object,
        qual,
      );
    }
  %}

hadZoneChange ->
  number __ object __ qualifiedZoneChange {%
    ([, , number, , object, , zoneChange]) => {
      return new HadZoneChangeStaticPlayerCondition(
        number,
        object,
        zoneChange,
      );
    }
  %}

hasAttacked ->
  "attacked" __ "this" __ "turn" {%
    () => new HasAttackedStaticPlayerCondition()
  %}

hasDesignation ->
  "the" __ "city's" __ "blessing" {%
    () => new HasDesignationStaticPlayerCondition(
      PlayerDesignation.CITYS_BLESSING,
    )
  %}

hasCounters ->
  number __ counter {%
    ([number, , counter]) => new HasCountersStaticPlayerCondition(
      number,
      counter,
    )
  %}

hasLife ->
  number __ "life" __ "more" __ "than" __ "your" __ "starting" __ "life" __ "total" {%
    ([, , number]) => {
      return new HasLifeStaticPlayerCondition(
        number,
        'startingLifeTotal',
      );
    }
  %}

gainedLife ->
  "gained" __ "life" __ "this" __ "turn" {%
    () => new GainedLifeStaticPlayerCondition()
  %}

countObjects ->
  "above" __ "{this}" {%
    () => {
      const qualifier: StaticObjectConditionQualifier = {
        type: StaticObjectConditionQualifierType.ABOVE_THIS
      }

      return new CountObjectsStaticObjectCondition(
        qualifier,
      );
    }
  %}
  | "in" __ player __ zone {%
    ([, , player, , zone]) => {
      const qualifier: StaticObjectConditionQualifier = {
        type: StaticObjectConditionQualifierType.IN_ZONE,
        player,
        zone,
      }

      return new CountObjectsStaticObjectCondition(
        qualifier,
      );
    }
  %}

diedThisTurn ->
  "died" __ "this" __ "turn" {%
    () => new DiedThisTurnStaticObjectCondition()
  %}

hasTotalPower ->
  "total" __ "power" __ number {%
    ([, , , , number]) => {
      return new HasTotalPowerStaticObjectCondition(number);
    }
  %}
  | "power" __ "is" __ number {%
    ([, , , , number]) => {
      return new HasTotalPowerStaticObjectCondition(number);
    }
  %}

isAttacking ->
  "attacking" {%
    () => new IsAttackingStaticObjectCondition()
  %}

player ->
  "you" {% () => Player.CONTROLLER %}
  | "your" {% () => Player.CONTROLLER %}
  | "defending" __ "player" {% () => Player.DEFENDING_PLAYER %}
  | ("an" __):? "opponent" {% () => Player.OPPONENT %}

additionalPermanentQualifier ->
  additionalCreatureQualifier {% id %}

additionalCreatureQualifier ->
  "with" __ "different" __ "powers" {%
    (): StaticPlayerConditionQualifier => ({
      type: StaticPlayerConditionQualifierType.WITH_DIFFERENT_POWERS,
    })
  %}
  | "with" __ "power" __ number {%
    ([, , , , number]): StaticPlayerConditionQualifier => ({
      type: StaticPlayerConditionQualifierType.WITH_POWER,
      number,
    })
  %}
  | "that" __ "fought" __ "this" __ "turn" {%
    (): StaticPlayerConditionQualifier => ({
      type: StaticPlayerConditionQualifierType.FOUGHT_THIS_TURN
    })
  %}
  | "with" __ keyword {%
    ([, , keyword]): StaticPlayerConditionQualifier => ({
      type: StaticPlayerConditionQualifierType.WITH_KEYWORD,
      keyword,
    })
  %}

# endregion

# region EFFECT

effect -> "effect" "." {% ([effect]): Effect => new Effect(EffectType.EFFECT) %}

# endregion

# region ZONE_CHANGE

qualifiedZoneChange ->
  zoneChange (__ zoneChangeQualifier):* {%
    ([change, qualifierList]): ZoneChange => {
      for (const [, qualifier] of qualifierList) {
        change.addQualifier(qualifier);
      }

      return change;
    }
  %}

zoneChangeQualifier ->
  "this" __ "turn" {%
    (): ZoneChangeQualifier => ({
      type: ZoneChangeQualifierType.THIS_TURN,
    })
  %}
  | "under" __ player __ "control" {%
    ([, , subject]): ZoneChangeQualifier => ({
      type: ZoneChangeQualifierType.UNDER_PLAYERS_CONTROL,
      subject
    })
  %}

zoneChange ->
  ("enter" | "enters") __ ("the" | player) __ zone {%
    ([, , [player], , zone]): EnterZoneChange => {
      const subject = player.text === 'the' ? Player.ALL : player;

      return new EnterZoneChange(zone, subject)
    }
  %}
  | ("leave" | "leaves") __ ("the" | player) __ zone {%
    ([, , [player], , zone]): LeaveZoneChange => {
      const subject = player.text === 'the' ? Player.ALL : player;

      return new LeaveZoneChange(zone, subject)
    }
  %}

zone ->
  "battlefield" {% (): Zone => Zone.BATTLEFIELD %}
  | "exile" {% (): Zone => Zone.EXILE %}
  | "graveyard" {% (): Zone => Zone.GRAVEYARD %}
  | "hand" {% (): Zone => Zone.HAND %}
  | "library" {% (): Zone => Zone.LIBRARY %}

# endregion

# region COST

costs ->
  singleCost ("," _ singleCost):* {%
    ([cost, costList]): Cost[] => ([
      cost,
      ...extractFromList(costList, 2)
    ])
  %}

singleCost ->
  manaCost {%
    ([cost]): ManaCost => cost
  %}
  | tapUntap {%
    ([cost]): TapUntap => cost
  %}
  | playerAction {%
    ([cost]): PlayerAction => cost
  %}

manaCost ->
  singleManaCost (_ singleManaCost):* {%
    ([cost, costList]): ManaCost => {
      const manaCost = [
        cost,
        ...extractFromList(costList, 1)
      ];

      return new ManaCost(manaCost);
    }
  %}

tapUntap -> (
  TAP
  | UNTAP
) {% ([[value]]): TapUntap => new TapUntap(value) %}

playerAction ->
  "pay" __ %number __ "life" {%
    ([, , number]) => {
      const num = new Number(parseInt(number.text, 10));

      return new PayLifePlayerAction(num);
    }
  %}
  | "sacrifice" __ (number __):? object {%
    ([, , number, object]) => {
      return new SacrificePlayerAction(
        object,
        extractFromOptional(number, 0) ?? undefined,
      );
    }
  %}
  | "remove" __ number __ "+1/+1" __ ("counter" | "counters") __ "from" __ object {%
    ([, , number, , , , , , , , object]) => {
      return new RemoveCountersPlayerAction(
        number,
        object,
      );
    }
  %}

# endregion

# region ACTIVATED_ABILITY_RESTRICTION

activationResctriction ->
  "activate" __ "only" __ activatedAbilityResctriction (("," __ | __ "and" __ "only" __) activatedAbilityResctriction):* %period {%
    ([, , , , restriction, restrictionList]): ActivatedAbilityRestriction[] => ([
      restriction,
      ...extractFromList(restrictionList, 1)
    ])
  %}

activatedAbilityResctriction ->
  priorityBasedRestriction {% id %}
  | stepBasedRestriction {% id %}
  | phaseBasedRestriction {% id %}
  | turnBasedRestriction {% id %}
  | conditionBasedRestriction {% id %}

priorityBasedRestriction ->
  "as" __ "a" __ "sorcery" {%
    (): PriorityBasedActivatedAbilityRestriction => {
      return new PriorityBasedActivatedAbilityRestriction(PriorityType.SORCERY_SPEED);
    }
  %}
  | "as" __ "an" __ "instant" {%
    (): PriorityBasedActivatedAbilityRestriction => {
      return new PriorityBasedActivatedAbilityRestriction(PriorityType.INSTANT_SPEED);
    }
  %}

stepBasedRestriction ->
  stepPhaseQualifier __ (player __):? step {%
    ([qualifier, , player, step]) => {
      return new StepBasedActivatedAbilityResctriction(
        qualifier,
        step,
        extractFromOptional(player, 0) ?? undefined,
      );
    }
  %}

phaseBasedRestriction ->
  "phase"

turnBasedRestriction ->
  "once" __ "each" __ "turn" {%
    () => {
      return new TurnBasedActivatedAbilityRestriction(
        StepPhaseQualifier.EACH
      )
    }
  %}
  | stepPhaseQualifier __ player __ "turn" {%
    ([qualifier, , player]) => {
      return new TurnBasedActivatedAbilityRestriction(
        qualifier,
        player
      )
    }
  %}

conditionBasedRestriction ->
  "if" __ condition {%
    ([, , condition]) => {
      return new ConditionBasedActivatedAbilityRestriction(
        condition,
      );
    }
  %}

stepPhaseQualifier ->
  "before" {% () => StepPhaseQualifier.BEFORE %}
  | "during" {% () => StepPhaseQualifier.DURING %}
  | "after" {% () => StepPhaseQualifier.AFTER %}

step ->
  stepName {% id %}
  | "the" __ stepName __ "step" {% ([, , step]) => step %}
  | stepDescriptor {% id %}

stepDescriptor ->
  "blockers" __ "are" __ "declared" {% () => CombatStep.DECLARE_BLOCKERS %}
  | "attackers" __ "are" __ "declared" {% () => CombatStep.DECLARE_ATTACKERS %}

stepName ->
  "combat" __ "damage" {% () => CombatStep.COMBAT_DAMAGE %}
  | "declare" __ "blockers" {% () => CombatStep.DECLARE_BLOCKERS %}
  | "upkeep" {% () => BeginningStep.UPKEEP %}

# endregion

# region COMMON

number ->
  "a" {% () => new Number(1) %}
  | "an" {% () => new Number(1) %}
  | (englishNumber | %number) (__ numberComparrisonSuffix):? {%
    ([[numToken], comparrison]) => {
      let num: number;

      if (typeof numToken === 'number') {
        num = numToken;
      } else {
        num = parseInt(numToken.text);
      }

      return new Number(
        num,
        extractFromOptional(comparrison, 1) ?? undefined,
      );
    }
  %}
  | numberComparrisonPrefix __ (englishNumber | %number) {%
    ([comparrison, , [numToken]]) => {
      let num: number;

      if (typeof numToken === 'number') {
        num = numToken;
      } else {
        num = parseInt(numToken.text);
      }

      return new Number(
        num,
        extractFromOptional(comparrison, 1) ?? undefined,
      );
    }
  %}

englishNumber ->
  "one" {% () => 1 %}
  | "two" {% () => 2 %}
  | "three" {% () => 3 %}
  | "four" {% () => 4 %}
  | "five" {% () => 5 %}
  | "six" {% () => 6 %}
  | "seven" {% () => 7 %}
  | "eight" {% () => 8 %}

numberComparrisonSuffix ->
  "or" __ ("more" | "greater") {% (): Comparrison => Comparrison.GTE %}

numberComparrisonPrefix ->
  "up" __ "to" {% (): Comparrison => Comparrison.LTE %}
  | "at" __ "least" {% (): Comparrison => Comparrison.GTE %}

# endregion

# region ENUMS

keywords ->
  keyword (("," | ";") __ keyword):* {%
    ([keyword, keywordList]): Keyword[] => ([
      keyword,
      ...extractFromList(keywordList, 2)
    ])
  %}

keyword -> (
  FLYING
  | BANDING
) {% ([[kw]]): Keyword => kw %}

counter ->
  counterType __ ("counter" | "counters") {%
    ([counter]) => counter
  %}

counterType ->
  "poison" {% () => Counter.POISON %}

# endregion
