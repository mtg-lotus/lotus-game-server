@preprocessor typescript

@{%
import moo from 'moo';

import {
  BasicMana,
  ManaCost as ManaCostEnum,
  CardType,
  SuperType,
  SubType,
  Keyword,
} from '@engine/classes/enums';

import {
	TapUntapCost,
} from '@engine/classes/cost';

const lexer = moo.compile({
  helpText: { match: /\([^)]+\)/, lineBreaks: true, value: s => s.toLowerCase() },
  word: { match: /[a-z!']+/, value: s => s.toLowerCase() },
  ws: /[ \t]+/,
  nl: { match: /[\r\n]+/, lineBreaks: true },
  symbol: /\{[^}]+\}/,
  hyphen: /-/,
  colon: /:/,
  period: /\./,
	comma: /,/,
  number: /[0-9]+/,
});

%}

@lexer lexer

# region MANA

singleManaCost -> (
  basicMana
  | X_GENERIC
	| Y_GENERIC
	| Z_GENERIC
	| ZERO_GENERIC
	| HALF_GENERIC
	| ONE_GENERIC
	| TWO_GENERIC
	| THREE_GENERIC
	| FOUR_GENERIC
	| FIVE_GENERIC
	| SIX_GENERIC
	| SEVEN_GENERIC
	| EIGHT_GENERIC
	| NINE_GENERIC
	| TEN_GENERIC
	| ELEVEN_GENERIC
	| TWELVE_GENERIC
	| THIRTEEN_GENERIC
	| FOURTEEN_GENERIC
	| FIFTEEN_GENERIC
	| SIXTEEN_GENERIC
	| SEVENTEEN_GENERIC
	| EIGHTEEN_GENERIC
	| NINETEEN_GENERIC
	| TWENTY_GENERIC
	| HUNDRED_GENERIC
	| MILLION_GENERIC
	| INFINITY_GENERIC
	| WHITE_BLUE
	| WHITE_BLACK
	| BLACK_RED
	| BLACK_GREEN
	| BLUE_BLACK
	| BLUE_RED
	| RED_GREEN
	| RED_WHITE
	| GREEN_WHITE
	| GREEN_BLUE
	| BLACK_GREEN_PHY
	| BLACK_RED_PHY
	| GREEN_BLUE_PHY
	| GREEN_WHITE_PHY
	| RED_GREEN_PHY
	| RED_WHITE_PHY
	| BLUE_BLACK_PHY
	| BLUE_RED_PHY
	| WHITE_BLACK_PHY
	| WHITE_BLUE_PHY
	| PHYREXIAN_GENERIC
	| WHITE_PHY
	| BLUE_PHY
	| BLACK_PHY
	| RED_PHY
	| GREEN_PHY
	| HALF_WHITE
	| HALF_RED
	| SNOW_MANA
) {% ([[cost]]) => cost %}

basicMana -> (
  COLORLESS
  | WHITE
  | BLUE
  | BLACK
  | RED
  | GREEN
) {% ([[cost]]) => cost %}

# endregion

# region TYPES

cardType -> (
  ARTIFACT
	| BATTLE
	| CREATURE
	| ENCHANTMENT
	| LAND
	| PLANESWALKER
	| INSTANT
	| SORCERY
) {% ([[type]]) => type %}

superType -> (
  BASIC
	| ELITE
	| HOST
	| LEGENDARY
	| ONGOING
	| SNOW
	| WORLD
) {% ([[type]]) => type %}

subType -> (
  artifactSubType
  | creatureSubType
  | enchantmentSubType
  | landSubType
  | planeswalkerSubType
  | instantSorcerySubType
) {% ([[type]]) => type %}

artifactSubType -> (
  ATTRACTION
	| BLOOD
	| CLUE
	| CONTRAPTION
	| EQUIPMENT
	| FOOD
	| FORTIFICATION
	| GOLD
	| KEY
	| POWERSTONE
	| TREASURE
) {% ([[type]]) => type %}

creatureSubType -> (
  ADVISOR
	| AETHERBORN
	| ALIEN
	| ALLY
	| ANGEL
	| ANTELOPE
	| APE
	| ARCHER
	| ARCHON
	| ARMY
	| ARTIFICER
	| ASSASSIN
	| ASSEMBLY_WORKER
	| ASTARTES
	| ATOG
	| AUROCHS
	| AVATAR
	| AZRA
	| BADGER
	| BALLOON
	| BARBARIAN
	| BARD
	| BASILISK
	| BAT
	| BEAR
	| BEAST
	| BEEBLE
	| BEHOLDER
	| BERSERKER
	| BIRD
	| BLINKMOTH
	| BOAR
	| BRINGER
	| BRUSHWAGG
	| CAMARID
	| CAMEL
	| CARIBOU
	| CARRIER
	| CAT
	| CENTUAR
	| CEPHALID
	| CHILD
	| CHIMERA
	| CITIZEN
	| CLERIC
	| CLOWN
	| COCKATRICE
	| CONSTRUCT
	| COWARD
	| CRAB
	| CROCODILE
	| CTAN
	| CUSTODES
	| CYCLOPS
	| DAUTHI
	| DEMIGOD
	| DEMON
	| DESERTER
	| DEVIL
	| DINOSAUR
	| DJINN
	| DOG
	| DRAGON
	| DRAKE
	| DREADNOUGHT
	| DRONE
	| DRUID
	| DRYAD
	| DWARF
	| EFREET
	| EGG
	| ELDER
	| ELDRAZI
	| ELEMENTAL
	| ELEPHANT
	| ELF
	| ELK
	| EMPLOYEE
	| FAERIE
	| FERRET
	| FISH
	| FLAGBEARER
	| FOX
	| FRACTAL
	| FROG
	| FUNGUS
	| GAMER
	| GARGOYLE
	| GERM
	| GIANT
	| GITH
	| GNOLL
	| GNOME
	| GOAT
	| GOBLIN
	| GOD
	| GOLEM
	| GORGON
	| GRAVEBORN
	| GREMLIN
	| GRIFFIN
	| GUEST
	| HAG
	| HALFLING
	| HAMSTER
	| HARPY
	| HELLION
	| HIPPO
	| HIPPOGRIFF
	| HOMARID
	| HOMUNCULUS
	| HORROR
	| HORSE
	| HUMAN
	| HYDRA
	| HYENA
	| ILLUSION
	| IMP
	| INCARNATION
	| INKLING
	| INQUISITOR
	| INSECT
	| JACKAL
	| JELLYFISH
	| JUGGERNAUT
	| KAVU
	| KIRIN
	| KITHKIN
	| KNIGHT
	| KOBOLD
	| KOR
	| KRAKEN
	| LAMIA
	| LAMMASU
	| LEECH
	| LEVIATHAN
	| LHURGOYF
	| LICID
	| LIZARD
	| MANTICORE
	| MASTICORE
	| MERCENARY
	| MERFOLK
	| METHATHRAN
	| MINION
	| MINOTAUR
	| MITE
	| MOLE
	| MONGER
	| MONGOOSE
	| MONK
	| MONKEY
	| MOONFOLK
	| MOUSE
	| MUTANT
	| MYR
	| MYSTIC
	| NAGA
	| NAUTILUS
	| NECRON
	| NEPHILIM
	| NIGHTMARE
	| NIGHTSTALKER
	| NINJA
	| NOBLE
	| NOGGLE
	| NOMAD
	| NYMPH
	| OCTOPUS
	| OGRE
	| OOZE
	| ORB
	| ORC
	| ORGG
	| OTTER
	| OUPHE
	| OX
	| OYSTER
	| PANGOLIN
	| PEASANT
	| PEGASUS
	| PENTAVITE
	| PERFORMER
	| PEST
	| PHELDDAGRIF
	| PHEONIX
	| PHYREXIAN
	| PILOT
	| PINCHER
	| PIRATE
	| PLANT
	| PRAETOR
	| PRIMARCH
	| PRISM
	| PROCESSOR
	| RACCOON
	| RABBIT
	| RANGER
	| RAT
	| REBEL
	| REFLECTION
	| RHINO
	| RIGGER
	| ROBOT
	| ROGUE
	| SABLE
	| SALAMANDER
	| SAMURAI
	| SAND
	| SAPORLING
	| SATYR
	| SCARECROW
	| SCION
	| SCORPION
	| SCOUT
	| SCULPTURE
	| SERF
	| SERPENT
	| SERVO
	| SHADE
	| SHAMAN
	| SHAPESHIFTER
	| SHARK
	| SHEEP
	| SIREN
	| SKELETON
	| SLITH
	| SLIVER
	| SLUG
	| SNAKE
	| SOLDIER
	| SOLTARI
	| SPAWN
	| SPECTER
	| SPELLSHAPER
	| SPHINX
	| SPIDER
	| SPIKE
	| SPIRIT
	| SPLINTER
	| SPONGE
	| SQUID
	| SQUIRREL
	| STARFISH
	| SURRAKAR
	| SURVIVOR
	| TENTACLE
	| TETRAVITE
	| THALAKOS
	| THOPTER
	| THRULL
	| TIEFLING
	| TREEFOLK
	| TRILOBITE
	| TRISKELAVITE
	| TROLL
	| TURTLE
	| TYRANID
	| UNICORN
	| VAMPIRE
	| VEDALKEN
	| VIASHINO
	| VOLVER
	| WALL
	| WALRUS
	| WARLOCK
	| WARRIOR
	| WEIRD
	| WEREFOLK
	| WHALE
	| WIZARD
	| WOLF
	| WOLVERINE
	| WOMBAT
	| WORM
	| WRAITH
	| WURM
	| YETI
	| ZOMBIE
	| ZUBERA
) {% ([[type]]) => type %}

enchantmentSubType -> (
  AURA
	| BACKGROUND
	| CARTOUCHE
	| CLASS
	| CURSE
	| RUNE
	| SAGA
	| SHARD
	| SHRINE
) {% ([[type]]) => type %}

landSubType -> (
  PLAINS
	| ISLAND
	| SWAMP
	| MOUNTAIN
	| FOREST
	| DESERT
	| GATE
	| LAIR
	| LOCUS
	| SPHERE
	| URZAS_MINE
	| URZAS_POWERPLANT
	| URZAS_TOWER
) {% ([[type]]) => type %}

planeswalkerSubType -> (
  AJANI
	| AMINATOU
	| ANGRATH
	| ARLINN
	| ASHIOK
	| BAHAMUT
	| BASRI
	| BOLAS
	| CALIX
	| CHANDRA
	| COMET
	| DACK
	| DAKKON
	| DARETTI
	| DAVRIEL
	| DIHADA
	| DOMRI
	| DOVIN
	| ELLYWICK
	| ELMINSTER
	| ELSPETH
	| ESTRID
	| FREYALISE
	| GARRUK
	| GIDEON
	| GRIST
	| HUATLI
	| JACE
	| JAYA
	| JARED
	| JESKA
	| KAITO
	| KARN
	| KASMINA
	| KAYA
	| KIORA
	| KOTH
	| LILIANA
	| LOLTH
	| LUKKA
	| MINSC
	| MORDENKAINEN
	| NAHIRI
	| NARSET
	| NIKO
	| NISSA
	| NIXILIS
	| OKO
	| RAL
	| ROWAN
	| SAHEELI
	| SAMUT
	| SARKHAN
	| SERRA
	| SIVITRI
	| SORIN
	| SZAT
	| TAMIYO
	| TASHA
	| TEFERI
	| TEYO
	| TEZZERET
	| TIBALT
	| TYVAR
	| UGIN
	| URZA
	| VENSER
	| VIVIEN
	| VRASKA
	| WILL
	| WINDGRACE
	| WRENN
	| XENAGOS
	| YANGGU
	| YANLING
	| ZARIEL
) {% ([[type]]) => type %}

instantSorcerySubType -> (
  ADVENTURE
  | ARCANE
  | LESSON
  | TRAP
) {% ([[type]]) => type %}

# endregion

_ -> %ws:?
__ -> %ws

# region MANA_TOKENS

COLORLESS -> "{c}" {% () => BasicMana.COLORLESS %}
WHITE -> "{w}" {% () => BasicMana.WHITE %}
BLUE -> "{u}" {% () => BasicMana.BLUE %}
BLACK -> "{b}" {% () => BasicMana.BLACK %}
RED -> "{r}" {% () => BasicMana.RED %}
GREEN -> "{g}" {% () => BasicMana.GREEN %}

X_GENERIC -> "{x}" {% () => ManaCostEnum.X_GENERIC %}
Y_GENERIC -> "{y}" {% () => ManaCostEnum.Y_GENERIC %}
Z_GENERIC -> "{z}" {% () => ManaCostEnum.Z_GENERIC %}
ZERO_GENERIC -> "{0}" {% () => ManaCostEnum.ZERO_GENERIC %}
HALF_GENERIC -> "{½}" {% () => ManaCostEnum.HALF_GENERIC %}
ONE_GENERIC -> "{1}" {% () => ManaCostEnum.ONE_GENERIC %}
TWO_GENERIC -> "{2}" {% () => ManaCostEnum.TWO_GENERIC %}
THREE_GENERIC -> "{3}" {% () => ManaCostEnum.THREE_GENERIC %}
FOUR_GENERIC -> "{4}" {% () => ManaCostEnum.FOUR_GENERIC %}
FIVE_GENERIC -> "{5}" {% () => ManaCostEnum.FIVE_GENERIC %}
SIX_GENERIC -> "{6}" {% () => ManaCostEnum.SIX_GENERIC %}
SEVEN_GENERIC -> "{7}" {% () => ManaCostEnum.SEVEN_GENERIC %}
EIGHT_GENERIC -> "{8}" {% () => ManaCostEnum.EIGHT_GENERIC %}
NINE_GENERIC -> "{9}" {% () => ManaCostEnum.NINE_GENERIC %}
TEN_GENERIC -> "{10}" {% () => ManaCostEnum.TEN_GENERIC %}
ELEVEN_GENERIC -> "{11}" {% () => ManaCostEnum.ELEVEN_GENERIC %}
TWELVE_GENERIC -> "{12}" {% () => ManaCostEnum.TWELVE_GENERIC %}
THIRTEEN_GENERIC -> "{13}" {% () => ManaCostEnum.THIRTEEN_GENERIC %}
FOURTEEN_GENERIC -> "{14}" {% () => ManaCostEnum.FOURTEEN_GENERIC %}
FIFTEEN_GENERIC -> "{15}" {% () => ManaCostEnum.FIFTEEN_GENERIC %}
SIXTEEN_GENERIC -> "{16}" {% () => ManaCostEnum.SIXTEEN_GENERIC %}
SEVENTEEN_GENERIC -> "{17}" {% () => ManaCostEnum.SEVENTEEN_GENERIC %}
EIGHTEEN_GENERIC -> "{18}" {% () => ManaCostEnum.EIGHTEEN_GENERIC %}
NINETEEN_GENERIC -> "{19}" {% () => ManaCostEnum.NINETEEN_GENERIC %}
TWENTY_GENERIC -> "{20}" {% () => ManaCostEnum.TWENTY_GENERIC %}
HUNDRED_GENERIC -> "{100}" {% () => ManaCostEnum.HUNDRED_GENERIC %}
MILLION_GENERIC -> "{1000000}" {% () => ManaCostEnum.MILLION_GENERIC %}
INFINITY_GENERIC -> "{∞}" {% () => ManaCostEnum.INFINITY_GENERIC %}
WHITE_BLUE -> "{w/u}" {% () => ManaCostEnum.WHITE_BLUE %}
WHITE_BLACK -> "{w/b}" {% () => ManaCostEnum.WHITE_BLACK %}
BLACK_RED -> "{b/r}" {% () => ManaCostEnum.BLACK_RED %}
BLACK_GREEN -> "{r/g}" {% () => ManaCostEnum.BLACK_GREEN %}
BLUE_BLACK -> "{u/b}" {% () => ManaCostEnum.BLUE_BLACK %}
BLUE_RED -> "{u/r}" {% () => ManaCostEnum.BLUE_RED %}
RED_GREEN -> "{r/g}" {% () => ManaCostEnum.RED_GREEN %}
RED_WHITE -> "{r/w}" {% () => ManaCostEnum.RED_WHITE %}
GREEN_WHITE -> "{g/w}" {% () => ManaCostEnum.GREEN_WHITE %}
GREEN_BLUE -> "{g/b}" {% () => ManaCostEnum.GREEN_BLUE %}
BLACK_GREEN_PHY -> "{b/g/p}" {% () => ManaCostEnum.BLACK_GREEN_PHY %}
BLACK_RED_PHY -> "{b/r/p}" {% () => ManaCostEnum.BLACK_RED_PHY %}
GREEN_BLUE_PHY -> "{g/u/p}" {% () => ManaCostEnum.GREEN_BLUE_PHY %}
GREEN_WHITE_PHY -> "{g/w/p}" {% () => ManaCostEnum.GREEN_WHITE_PHY %}
RED_GREEN_PHY -> "{r/g/p}" {% () => ManaCostEnum.RED_GREEN_PHY %}
RED_WHITE_PHY -> "{r/w/p}" {% () => ManaCostEnum.RED_WHITE_PHY %}
BLUE_BLACK_PHY -> "{u/b/p}" {% () => ManaCostEnum.BLUE_BLACK_PHY %}
BLUE_RED_PHY -> "{u/r/p}" {% () => ManaCostEnum.BLUE_RED_PHY %}
WHITE_BLACK_PHY -> "{w/b/p}" {% () => ManaCostEnum.WHITE_BLACK_PHY %}
WHITE_BLUE_PHY -> "{w/u/p}" {% () => ManaCostEnum.WHITE_BLUE_PHY %}
PHYREXIAN_GENERIC -> "{p}" {% () => ManaCostEnum.PHYREXIAN_GENERIC %}
WHITE_PHY -> "{w/p}" {% () => ManaCostEnum.WHITE_PHY %}
BLUE_PHY -> "{u/p}" {% () => ManaCostEnum.BLUE_PHY %}
BLACK_PHY -> "{b/p}" {% () => ManaCostEnum.BLACK_PHY %}
RED_PHY -> "{r/p}" {% () => ManaCostEnum.RED_PHY %}
GREEN_PHY -> "{g/p}" {% () => ManaCostEnum.GREEN_PHY %}
HALF_WHITE -> "{hw}" {% () => ManaCostEnum.HALF_WHITE %}
HALF_RED -> "{hr}" {% () => ManaCostEnum.HALF_RED %}
SNOW_MANA -> "{s}" {% () => ManaCostEnum.SNOW_MANA %}

#endregion MANA_TOKENS

TAP -> "{t}" {% () => TapUntapCost.TAP %}
UNTAP -> "{q}" {% () => TapUntapCost.UNTAP %}

# region TYPE_TOKENS

ARTIFACT -> ("artifact" | "artifacts") {% () => CardType.ARTIFACT %}
BATTLE -> ("battle" | "battles") {% () => CardType.BATTLE %}
CREATURE -> ("creature" | "creatures") {% () => CardType.CREATURE %}
ENCHANTMENT -> ("enchantment" | "enchantments") {% () => CardType.ENCHANTMENT %}
LAND -> ("land" | "lands") {% () => CardType.LAND %}
PLANESWALKER -> ("planeswalker" | "planeswalkers") {% () => CardType.PLANESWALKER %}
INSTANT -> ("instant" | "instants") {% () => CardType.INSTANT %}
SORCERY -> ("sorcery" | "sorceries") {% () => CardType.SORCERY %}

BASIC -> ("basic" | "basics") {% () => SuperType.BASIC %}
ELITE -> ("elite" | "elites") {% () => SuperType.ELITE %}
HOST -> ("host" | "hosts") {% () => SuperType.HOST %}
LEGENDARY -> ("legendary" | "legendaries") {% () => SuperType.LEGENDARY %}
ONGOING -> ("ongoing" | "ongoings") {% () => SuperType.ONGOING %}
SNOW -> ("snow" | "snows") {% () => SuperType.SNOW %}
WORLD -> ("world" | "worlds") {% () => SuperType.WORLD %}

ATTRACTION -> ("attraction" | "attractions") {% () => SubType.ATTRACTION %}
BLOOD -> ("blood" | "bloods") {% () => SubType.BLOOD %}
CLUE -> ("clue" | "clues") {% () => SubType.CLUE %}
CONTRAPTION -> ("contraption" | "contraptions") {% () => SubType.CONTRAPTION %}
EQUIPMENT -> ("equipment" | "equipments") {% () => SubType.EQUIPMENT %}
FOOD -> ("food" | "foods") {% () => SubType.FOOD %}
FORTIFICATION -> ("fortification" | "fortifications") {% () => SubType.FORTIFICATION %}
GOLD -> ("gold" | "golds") {% () => SubType.GOLD %}
KEY -> ("key" | "keies") {% () => SubType.KEY %}
POWERSTONE -> ("powerstone" | "powerstones") {% () => SubType.POWERSTONE %}
TREASURE -> ("treasure" | "treasures") {% () => SubType.TREASURE %}
ADVISOR -> ("advisor" | "advisors") {% () => SubType.ADVISOR %}
AETHERBORN -> ("aetherborn" | "aetherborns") {% () => SubType.AETHERBORN %}
ALIEN -> ("alien" | "aliens") {% () => SubType.ALIEN %}
ALLY -> ("ally" | "allies") {% () => SubType.ALLY %}
ANGEL -> ("angel" | "angels") {% () => SubType.ANGEL %}
ANTELOPE -> ("antelope" | "antelopes") {% () => SubType.ANTELOPE %}
APE -> ("ape" | "apes") {% () => SubType.APE %}
ARCHER -> ("archer" | "archers") {% () => SubType.ARCHER %}
ARCHON -> ("archon" | "archons") {% () => SubType.ARCHON %}
ARMY -> ("army" | "armies") {% () => SubType.ARMY %}
ARTIFICER -> ("artificer" | "artificers") {% () => SubType.ARTIFICER %}
ASSASSIN -> ("assassin" | "assassins") {% () => SubType.ASSASSIN %}
ASSEMBLY_WORKER -> "assembly-worker" {% () => SubType.ASSEMBLY_WORKER %}
ASTARTES -> "astartes" {% () => SubType.ASTARTES %}
ATOG -> ("atog" | "atogs") {% () => SubType.ATOG %}
AUROCHS -> "aurochs" {% () => SubType.AUROCHS %}
AVATAR -> ("avatar" | "avatars") {% () => SubType.AVATAR %}
AZRA -> ("azra" | "azras") {% () => SubType.AZRA %}
BADGER -> ("badger" | "badgers") {% () => SubType.BADGER %}
BALLOON -> ("balloon" | "balloons") {% () => SubType.BALLOON %}
BARBARIAN -> ("barbarian" | "barbarians") {% () => SubType.BARBARIAN %}
BARD -> ("bard" | "bards") {% () => SubType.BARD %}
BASILISK -> ("basilisk" | "basilisks") {% () => SubType.BASILISK %}
BAT -> ("bat" | "bats") {% () => SubType.BAT %}
BEAR -> ("bear" | "bears") {% () => SubType.BEAR %}
BEAST -> ("beast" | "beasts") {% () => SubType.BEAST %}
BEEBLE -> ("beeble" | "beebles") {% () => SubType.BEEBLE %}
BEHOLDER -> ("beholder" | "beholders") {% () => SubType.BEHOLDER %}
BERSERKER -> ("berserker" | "berserkers") {% () => SubType.BERSERKER %}
BIRD -> ("bird" | "birds") {% () => SubType.BIRD %}
BLINKMOTH -> ("blinkmoth" | "blinkmoths") {% () => SubType.BLINKMOTH %}
BOAR -> ("boar" | "boars") {% () => SubType.BOAR %}
BRINGER -> ("bringer" | "bringers") {% () => SubType.BRINGER %}
BRUSHWAGG -> ("brushwagg" | "brushwaggs") {% () => SubType.BRUSHWAGG %}
CAMARID -> ("camarid" | "camarids") {% () => SubType.CAMARID %}
CAMEL -> ("camel" | "camels") {% () => SubType.CAMEL %}
CARIBOU -> ("caribou" | "caribous") {% () => SubType.CARIBOU %}
CARRIER -> ("carrier" | "carriers") {% () => SubType.CARRIER %}
CAT -> ("cat" | "cats") {% () => SubType.CAT %}
CENTUAR -> ("centuar" | "centuars") {% () => SubType.CENTUAR %}
CEPHALID -> ("cephalid" | "cephalids") {% () => SubType.CEPHALID %}
CHILD -> ("child" | "childs") {% () => SubType.CHILD %}
CHIMERA -> ("chimera" | "chimeras") {% () => SubType.CHIMERA %}
CITIZEN -> ("citizen" | "citizens") {% () => SubType.CITIZEN %}
CLERIC -> ("cleric" | "clerics") {% () => SubType.CLERIC %}
CLOWN -> ("clown" | "clowns") {% () => SubType.CLOWN %}
COCKATRICE -> ("cockatrice" | "cockatrices") {% () => SubType.COCKATRICE %}
CONSTRUCT -> ("construct" | "constructs") {% () => SubType.CONSTRUCT %}
COWARD -> ("coward" | "cowards") {% () => SubType.COWARD %}
CRAB -> ("crab" | "crabs") {% () => SubType.CRAB %}
CROCODILE -> ("crocodile" | "crocodiles") {% () => SubType.CROCODILE %}
CTAN -> ("ctan" | "ctans") {% () => SubType.CTAN %}
CUSTODES -> "custodes" {% () => SubType.CUSTODES %}
CYCLOPS -> "cyclops" {% () => SubType.CYCLOPS %}
DAUTHI -> ("dauthi" | "dauthis") {% () => SubType.DAUTHI %}
DEMIGOD -> ("demigod" | "demigods") {% () => SubType.DEMIGOD %}
DEMON -> ("demon" | "demons") {% () => SubType.DEMON %}
DESERTER -> ("deserter" | "deserters") {% () => SubType.DESERTER %}
DEVIL -> ("devil" | "devils") {% () => SubType.DEVIL %}
DINOSAUR -> ("dinosaur" | "dinosaurs") {% () => SubType.DINOSAUR %}
DJINN -> ("djinn" | "djinns") {% () => SubType.DJINN %}
DOG -> ("dog" | "dogs") {% () => SubType.DOG %}
DRAGON -> ("dragon" | "dragons") {% () => SubType.DRAGON %}
DRAKE -> ("drake" | "drakes") {% () => SubType.DRAKE %}
DREADNOUGHT -> ("dreadnought" | "dreadnoughts") {% () => SubType.DREADNOUGHT %}
DRONE -> ("drone" | "drones") {% () => SubType.DRONE %}
DRUID -> ("druid" | "druids") {% () => SubType.DRUID %}
DRYAD -> ("dryad" | "dryads") {% () => SubType.DRYAD %}
DWARF -> ("dwarf" | "dwarfs") {% () => SubType.DWARF %}
EFREET -> ("efreet" | "efreets") {% () => SubType.EFREET %}
EGG -> ("egg" | "eggs") {% () => SubType.EGG %}
ELDER -> ("elder" | "elders") {% () => SubType.ELDER %}
ELDRAZI -> ("eldrazi" | "eldrazis") {% () => SubType.ELDRAZI %}
ELEMENTAL -> ("elemental" | "elementals") {% () => SubType.ELEMENTAL %}
ELEPHANT -> ("elephant" | "elephants") {% () => SubType.ELEPHANT %}
ELF -> ("elf" | "elfs") {% () => SubType.ELF %}
ELK -> ("elk" | "elks") {% () => SubType.ELK %}
EMPLOYEE -> ("employee" | "employees") {% () => SubType.EMPLOYEE %}
FAERIE -> ("faerie" | "faeries") {% () => SubType.FAERIE %}
FERRET -> ("ferret" | "ferrets") {% () => SubType.FERRET %}
FISH -> ("fish" | "fishs") {% () => SubType.FISH %}
FLAGBEARER -> ("flagbearer" | "flagbearers") {% () => SubType.FLAGBEARER %}
FOX -> ("fox" | "foxs") {% () => SubType.FOX %}
FRACTAL -> ("fractal" | "fractals") {% () => SubType.FRACTAL %}
FROG -> ("frog" | "frogs") {% () => SubType.FROG %}
FUNGUS -> "fungus" {% () => SubType.FUNGUS %}
GAMER -> ("gamer" | "gamers") {% () => SubType.GAMER %}
GARGOYLE -> ("gargoyle" | "gargoyles") {% () => SubType.GARGOYLE %}
GERM -> ("germ" | "germs") {% () => SubType.GERM %}
GIANT -> ("giant" | "giants") {% () => SubType.GIANT %}
GITH -> ("gith" | "giths") {% () => SubType.GITH %}
GNOLL -> ("gnoll" | "gnolls") {% () => SubType.GNOLL %}
GNOME -> ("gnome" | "gnomes") {% () => SubType.GNOME %}
GOAT -> ("goat" | "goats") {% () => SubType.GOAT %}
GOBLIN -> ("goblin" | "goblins") {% () => SubType.GOBLIN %}
GOD -> ("god" | "gods") {% () => SubType.GOD %}
GOLEM -> ("golem" | "golems") {% () => SubType.GOLEM %}
GORGON -> ("gorgon" | "gorgons") {% () => SubType.GORGON %}
GRAVEBORN -> ("graveborn" | "graveborns") {% () => SubType.GRAVEBORN %}
GREMLIN -> ("gremlin" | "gremlins") {% () => SubType.GREMLIN %}
GRIFFIN -> ("griffin" | "griffins") {% () => SubType.GRIFFIN %}
GUEST -> ("guest" | "guests") {% () => SubType.GUEST %}
HAG -> ("hag" | "hags") {% () => SubType.HAG %}
HALFLING -> ("halfling" | "halflings") {% () => SubType.HALFLING %}
HAMSTER -> ("hamster" | "hamsters") {% () => SubType.HAMSTER %}
HARPY -> ("harpy" | "harpies") {% () => SubType.HARPY %}
HELLION -> ("hellion" | "hellions") {% () => SubType.HELLION %}
HIPPO -> ("hippo" | "hippos") {% () => SubType.HIPPO %}
HIPPOGRIFF -> ("hippogriff" | "hippogriffs") {% () => SubType.HIPPOGRIFF %}
HOMARID -> ("homarid" | "homarids") {% () => SubType.HOMARID %}
HOMUNCULUS -> "homunculus" {% () => SubType.HOMUNCULUS %}
HORROR -> ("horror" | "horrors") {% () => SubType.HORROR %}
HORSE -> ("horse" | "horses") {% () => SubType.HORSE %}
HUMAN -> ("human" | "humans") {% () => SubType.HUMAN %}
HYDRA -> ("hydra" | "hydras") {% () => SubType.HYDRA %}
HYENA -> ("hyena" | "hyenas") {% () => SubType.HYENA %}
ILLUSION -> ("illusion" | "illusions") {% () => SubType.ILLUSION %}
IMP -> ("imp" | "imps") {% () => SubType.IMP %}
INCARNATION -> ("incarnation" | "incarnations") {% () => SubType.INCARNATION %}
INKLING -> ("inkling" | "inklings") {% () => SubType.INKLING %}
INQUISITOR -> ("inquisitor" | "inquisitors") {% () => SubType.INQUISITOR %}
INSECT -> ("insect" | "insects") {% () => SubType.INSECT %}
JACKAL -> ("jackal" | "jackals") {% () => SubType.JACKAL %}
JELLYFISH -> ("jellyfish" | "jellyfishs") {% () => SubType.JELLYFISH %}
JUGGERNAUT -> ("juggernaut" | "juggernauts") {% () => SubType.JUGGERNAUT %}
KAVU -> ("kavu" | "kavus") {% () => SubType.KAVU %}
KIRIN -> ("kirin" | "kirins") {% () => SubType.KIRIN %}
KITHKIN -> ("kithkin" | "kithkins") {% () => SubType.KITHKIN %}
KNIGHT -> ("knight" | "knights") {% () => SubType.KNIGHT %}
KOBOLD -> ("kobold" | "kobolds") {% () => SubType.KOBOLD %}
KOR -> ("kor" | "kors") {% () => SubType.KOR %}
KRAKEN -> ("kraken" | "krakens") {% () => SubType.KRAKEN %}
LAMIA -> ("lamia" | "lamias") {% () => SubType.LAMIA %}
LAMMASU -> ("lammasu" | "lammasus") {% () => SubType.LAMMASU %}
LEECH -> ("leech" | "leechs") {% () => SubType.LEECH %}
LEVIATHAN -> ("leviathan" | "leviathans") {% () => SubType.LEVIATHAN %}
LHURGOYF -> ("lhurgoyf" | "lhurgoyfs") {% () => SubType.LHURGOYF %}
LICID -> ("licid" | "licids") {% () => SubType.LICID %}
LIZARD -> ("lizard" | "lizards") {% () => SubType.LIZARD %}
MANTICORE -> ("manticore" | "manticores") {% () => SubType.MANTICORE %}
MASTICORE -> ("masticore" | "masticores") {% () => SubType.MASTICORE %}
MERCENARY -> ("mercenary" | "mercenaries") {% () => SubType.MERCENARY %}
MERFOLK -> ("merfolk" | "merfolks") {% () => SubType.MERFOLK %}
METHATHRAN -> ("methathran" | "methathrans") {% () => SubType.METHATHRAN %}
MINION -> ("minion" | "minions") {% () => SubType.MINION %}
MINOTAUR -> ("minotaur" | "minotaurs") {% () => SubType.MINOTAUR %}
MITE -> ("mite" | "mites") {% () => SubType.MITE %}
MOLE -> ("mole" | "moles") {% () => SubType.MOLE %}
MONGER -> ("monger" | "mongers") {% () => SubType.MONGER %}
MONGOOSE -> ("mongoose" | "mongooses") {% () => SubType.MONGOOSE %}
MONK -> ("monk" | "monks") {% () => SubType.MONK %}
MONKEY -> ("monkey" | "monkeies") {% () => SubType.MONKEY %}
MOONFOLK -> ("moonfolk" | "moonfolks") {% () => SubType.MOONFOLK %}
MOUSE -> ("mouse" | "mouses") {% () => SubType.MOUSE %}
MUTANT -> ("mutant" | "mutants") {% () => SubType.MUTANT %}
MYR -> ("myr" | "myrs") {% () => SubType.MYR %}
MYSTIC -> ("mystic" | "mystics") {% () => SubType.MYSTIC %}
NAGA -> ("naga" | "nagas") {% () => SubType.NAGA %}
NAUTILUS -> "nautilus" {% () => SubType.NAUTILUS %}
NECRON -> ("necron" | "necrons") {% () => SubType.NECRON %}
NEPHILIM -> ("nephilim" | "nephilims") {% () => SubType.NEPHILIM %}
NIGHTMARE -> ("nightmare" | "nightmares") {% () => SubType.NIGHTMARE %}
NIGHTSTALKER -> ("nightstalker" | "nightstalkers") {% () => SubType.NIGHTSTALKER %}
NINJA -> ("ninja" | "ninjas") {% () => SubType.NINJA %}
NOBLE -> ("noble" | "nobles") {% () => SubType.NOBLE %}
NOGGLE -> ("noggle" | "noggles") {% () => SubType.NOGGLE %}
NOMAD -> ("nomad" | "nomads") {% () => SubType.NOMAD %}
NYMPH -> ("nymph" | "nymphs") {% () => SubType.NYMPH %}
OCTOPUS -> "octopus" {% () => SubType.OCTOPUS %}
OGRE -> ("ogre" | "ogres") {% () => SubType.OGRE %}
OOZE -> ("ooze" | "oozes") {% () => SubType.OOZE %}
ORB -> ("orb" | "orbs") {% () => SubType.ORB %}
ORC -> ("orc" | "orcs") {% () => SubType.ORC %}
ORGG -> ("orgg" | "orggs") {% () => SubType.ORGG %}
OTTER -> ("otter" | "otters") {% () => SubType.OTTER %}
OUPHE -> ("ouphe" | "ouphes") {% () => SubType.OUPHE %}
OX -> ("ox" | "oxs") {% () => SubType.OX %}
OYSTER -> ("oyster" | "oysters") {% () => SubType.OYSTER %}
PANGOLIN -> ("pangolin" | "pangolins") {% () => SubType.PANGOLIN %}
PEASANT -> ("peasant" | "peasants") {% () => SubType.PEASANT %}
PEGASUS -> "pegasus" {% () => SubType.PEGASUS %}
PENTAVITE -> ("pentavite" | "pentavites") {% () => SubType.PENTAVITE %}
PERFORMER -> ("performer" | "performers") {% () => SubType.PERFORMER %}
PEST -> ("pest" | "pests") {% () => SubType.PEST %}
PHELDDAGRIF -> ("phelddagrif" | "phelddagrifs") {% () => SubType.PHELDDAGRIF %}
PHEONIX -> ("pheonix" | "pheonixs") {% () => SubType.PHEONIX %}
PHYREXIAN -> ("phyrexian" | "phyrexians") {% () => SubType.PHYREXIAN %}
PILOT -> ("pilot" | "pilots") {% () => SubType.PILOT %}
PINCHER -> ("pincher" | "pinchers") {% () => SubType.PINCHER %}
PIRATE -> ("pirate" | "pirates") {% () => SubType.PIRATE %}
PLANT -> ("plant" | "plants") {% () => SubType.PLANT %}
PRAETOR -> ("praetor" | "praetors") {% () => SubType.PRAETOR %}
PRIMARCH -> ("primarch" | "primarchs") {% () => SubType.PRIMARCH %}
PRISM -> ("prism" | "prisms") {% () => SubType.PRISM %}
PROCESSOR -> ("processor" | "processors") {% () => SubType.PROCESSOR %}
RACCOON -> ("raccoon" | "raccoons") {% () => SubType.RACCOON %}
RABBIT -> ("rabbit" | "rabbits") {% () => SubType.RABBIT %}
RANGER -> ("ranger" | "rangers") {% () => SubType.RANGER %}
RAT -> ("rat" | "rats") {% () => SubType.RAT %}
REBEL -> ("rebel" | "rebels") {% () => SubType.REBEL %}
REFLECTION -> ("reflection" | "reflections") {% () => SubType.REFLECTION %}
RHINO -> ("rhino" | "rhinos") {% () => SubType.RHINO %}
RIGGER -> ("rigger" | "riggers") {% () => SubType.RIGGER %}
ROBOT -> ("robot" | "robots") {% () => SubType.ROBOT %}
ROGUE -> ("rogue" | "rogues") {% () => SubType.ROGUE %}
SABLE -> ("sable" | "sables") {% () => SubType.SABLE %}
SALAMANDER -> ("salamander" | "salamanders") {% () => SubType.SALAMANDER %}
SAMURAI -> ("samurai" | "samurais") {% () => SubType.SAMURAI %}
SAND -> ("sand" | "sands") {% () => SubType.SAND %}
SAPORLING -> ("saporling" | "saporlings") {% () => SubType.SAPORLING %}
SATYR -> ("satyr" | "satyrs") {% () => SubType.SATYR %}
SCARECROW -> ("scarecrow" | "scarecrows") {% () => SubType.SCARECROW %}
SCION -> ("scion" | "scions") {% () => SubType.SCION %}
SCORPION -> ("scorpion" | "scorpions") {% () => SubType.SCORPION %}
SCOUT -> ("scout" | "scouts") {% () => SubType.SCOUT %}
SCULPTURE -> ("sculpture" | "sculptures") {% () => SubType.SCULPTURE %}
SERF -> ("serf" | "serfs") {% () => SubType.SERF %}
SERPENT -> ("serpent" | "serpents") {% () => SubType.SERPENT %}
SERVO -> ("servo" | "servos") {% () => SubType.SERVO %}
SHADE -> ("shade" | "shades") {% () => SubType.SHADE %}
SHAMAN -> ("shaman" | "shamans") {% () => SubType.SHAMAN %}
SHAPESHIFTER -> ("shapeshifter" | "shapeshifters") {% () => SubType.SHAPESHIFTER %}
SHARK -> ("shark" | "sharks") {% () => SubType.SHARK %}
SHEEP -> ("sheep" | "sheeps") {% () => SubType.SHEEP %}
SIREN -> ("siren" | "sirens") {% () => SubType.SIREN %}
SKELETON -> ("skeleton" | "skeletons") {% () => SubType.SKELETON %}
SLITH -> ("slith" | "sliths") {% () => SubType.SLITH %}
SLIVER -> ("sliver" | "slivers") {% () => SubType.SLIVER %}
SLUG -> ("slug" | "slugs") {% () => SubType.SLUG %}
SNAKE -> ("snake" | "snakes") {% () => SubType.SNAKE %}
SOLDIER -> ("soldier" | "soldiers") {% () => SubType.SOLDIER %}
SOLTARI -> ("soltari" | "soltaris") {% () => SubType.SOLTARI %}
SPAWN -> ("spawn" | "spawns") {% () => SubType.SPAWN %}
SPECTER -> ("specter" | "specters") {% () => SubType.SPECTER %}
SPELLSHAPER -> ("spellshaper" | "spellshapers") {% () => SubType.SPELLSHAPER %}
SPHINX -> ("sphinx" | "sphinxs") {% () => SubType.SPHINX %}
SPIDER -> ("spider" | "spiders") {% () => SubType.SPIDER %}
SPIKE -> ("spike" | "spikes") {% () => SubType.SPIKE %}
SPIRIT -> ("spirit" | "spirits") {% () => SubType.SPIRIT %}
SPLINTER -> ("splinter" | "splinters") {% () => SubType.SPLINTER %}
SPONGE -> ("sponge" | "sponges") {% () => SubType.SPONGE %}
SQUID -> ("squid" | "squids") {% () => SubType.SQUID %}
SQUIRREL -> ("squirrel" | "squirrels") {% () => SubType.SQUIRREL %}
STARFISH -> ("starfish" | "starfishs") {% () => SubType.STARFISH %}
SURRAKAR -> ("surrakar" | "surrakars") {% () => SubType.SURRAKAR %}
SURVIVOR -> ("survivor" | "survivors") {% () => SubType.SURVIVOR %}
TENTACLE -> ("tentacle" | "tentacles") {% () => SubType.TENTACLE %}
TETRAVITE -> ("tetravite" | "tetravites") {% () => SubType.TETRAVITE %}
THALAKOS -> "thalakos" {% () => SubType.THALAKOS %}
THOPTER -> ("thopter" | "thopters") {% () => SubType.THOPTER %}
THRULL -> ("thrull" | "thrulls") {% () => SubType.THRULL %}
TIEFLING -> ("tiefling" | "tieflings") {% () => SubType.TIEFLING %}
TREEFOLK -> ("treefolk" | "treefolks") {% () => SubType.TREEFOLK %}
TRILOBITE -> ("trilobite" | "trilobites") {% () => SubType.TRILOBITE %}
TRISKELAVITE -> ("triskelavite" | "triskelavites") {% () => SubType.TRISKELAVITE %}
TROLL -> ("troll" | "trolls") {% () => SubType.TROLL %}
TURTLE -> ("turtle" | "turtles") {% () => SubType.TURTLE %}
TYRANID -> ("tyranid" | "tyranids") {% () => SubType.TYRANID %}
UNICORN -> ("unicorn" | "unicorns") {% () => SubType.UNICORN %}
VAMPIRE -> ("vampire" | "vampires") {% () => SubType.VAMPIRE %}
VEDALKEN -> ("vedalken" | "vedalkens") {% () => SubType.VEDALKEN %}
VIASHINO -> ("viashino" | "viashinos") {% () => SubType.VIASHINO %}
VOLVER -> ("volver" | "volvers") {% () => SubType.VOLVER %}
WALL -> ("wall" | "walls") {% () => SubType.WALL %}
WALRUS -> "walrus" {% () => SubType.WALRUS %}
WARLOCK -> ("warlock" | "warlocks") {% () => SubType.WARLOCK %}
WARRIOR -> ("warrior" | "warriors") {% () => SubType.WARRIOR %}
WEIRD -> ("weird" | "weirds") {% () => SubType.WEIRD %}
WEREFOLK -> ("werefolk" | "werefolks") {% () => SubType.WEREFOLK %}
WHALE -> ("whale" | "whales") {% () => SubType.WHALE %}
WIZARD -> ("wizard" | "wizards") {% () => SubType.WIZARD %}
WOLF -> ("wolf" | "wolfs") {% () => SubType.WOLF %}
WOLVERINE -> ("wolverine" | "wolverines") {% () => SubType.WOLVERINE %}
WOMBAT -> ("wombat" | "wombats") {% () => SubType.WOMBAT %}
WORM -> ("worm" | "worms") {% () => SubType.WORM %}
WRAITH -> ("wraith" | "wraiths") {% () => SubType.WRAITH %}
WURM -> ("wurm" | "wurms") {% () => SubType.WURM %}
YETI -> ("yeti" | "yetis") {% () => SubType.YETI %}
ZOMBIE -> ("zombie" | "zombies") {% () => SubType.ZOMBIE %}
ZUBERA -> ("zubera" | "zuberas") {% () => SubType.ZUBERA %}
AURA -> ("aura" | "auras") {% () => SubType.AURA %}
BACKGROUND -> ("background" | "backgrounds") {% () => SubType.BACKGROUND %}
CARTOUCHE -> ("cartouche" | "cartouches") {% () => SubType.CARTOUCHE %}
CLASS -> "class" {% () => SubType.CLASS %}
CURSE -> ("curse" | "curses") {% () => SubType.CURSE %}
RUNE -> ("rune" | "runes") {% () => SubType.RUNE %}
SAGA -> ("saga" | "sagas") {% () => SubType.SAGA %}
SHARD -> ("shard" | "shards") {% () => SubType.SHARD %}
SHRINE -> ("shrine" | "shrines") {% () => SubType.SHRINE %}
PLAINS -> "plains" {% () => SubType.PLAINS %}
ISLAND -> ("island" | "islands") {% () => SubType.ISLAND %}
SWAMP -> ("swamp" | "swamps") {% () => SubType.SWAMP %}
MOUNTAIN -> ("mountain" | "mountains") {% () => SubType.MOUNTAIN %}
FOREST -> ("forest" | "forests") {% () => SubType.FOREST %}
DESERT -> ("desert" | "deserts") {% () => SubType.DESERT %}
GATE -> ("gate" | "gates") {% () => SubType.GATE %}
LAIR -> ("lair" | "lairs") {% () => SubType.LAIR %}
LOCUS -> "locus" {% () => SubType.LOCUS %}
SPHERE -> ("sphere" | "spheres") {% () => SubType.SPHERE %}
URZAS_MINE -> ("urza's mine" | "urza's mines") {% () => SubType.URZAS_MINE %}
URZAS_POWERPLANT -> ("urza's power-plant" | "urza's power-plants") {% () => SubType.URZAS_POWERPLANT %}
URZAS_TOWER -> ("urza's tower" | "urza's towers") {% () => SubType.URZAS_TOWER %}
AJANI -> ("ajani" | "ajanis") {% () => SubType.AJANI %}
AMINATOU -> ("aminatou" | "aminatous") {% () => SubType.AMINATOU %}
ANGRATH -> ("angrath" | "angraths") {% () => SubType.ANGRATH %}
ARLINN -> ("arlinn" | "arlinns") {% () => SubType.ARLINN %}
ASHIOK -> ("ashiok" | "ashioks") {% () => SubType.ASHIOK %}
BAHAMUT -> ("bahamut" | "bahamuts") {% () => SubType.BAHAMUT %}
BASRI -> ("basri" | "basris") {% () => SubType.BASRI %}
BOLAS -> "bolas" {% () => SubType.BOLAS %}
CALIX -> ("calix" | "calixs") {% () => SubType.CALIX %}
CHANDRA -> ("chandra" | "chandras") {% () => SubType.CHANDRA %}
COMET -> ("comet" | "comets") {% () => SubType.COMET %}
DACK -> ("dack" | "dacks") {% () => SubType.DACK %}
DAKKON -> ("dakkon" | "dakkons") {% () => SubType.DAKKON %}
DARETTI -> ("daretti" | "darettis") {% () => SubType.DARETTI %}
DAVRIEL -> ("davriel" | "davriels") {% () => SubType.DAVRIEL %}
DIHADA -> ("dihada" | "dihadas") {% () => SubType.DIHADA %}
DOMRI -> ("domri" | "domris") {% () => SubType.DOMRI %}
DOVIN -> ("dovin" | "dovins") {% () => SubType.DOVIN %}
ELLYWICK -> ("ellywick" | "ellywicks") {% () => SubType.ELLYWICK %}
ELMINSTER -> ("elminster" | "elminsters") {% () => SubType.ELMINSTER %}
ELSPETH -> ("elspeth" | "elspeths") {% () => SubType.ELSPETH %}
ESTRID -> ("estrid" | "estrids") {% () => SubType.ESTRID %}
FREYALISE -> ("freyalise" | "freyalises") {% () => SubType.FREYALISE %}
GARRUK -> ("garruk" | "garruks") {% () => SubType.GARRUK %}
GIDEON -> ("gideon" | "gideons") {% () => SubType.GIDEON %}
GRIST -> ("grist" | "grists") {% () => SubType.GRIST %}
HUATLI -> ("huatli" | "huatlis") {% () => SubType.HUATLI %}
JACE -> ("jace" | "jaces") {% () => SubType.JACE %}
JAYA -> ("jaya" | "jayas") {% () => SubType.JAYA %}
JARED -> ("jared" | "jareds") {% () => SubType.JARED %}
JESKA -> ("jeska" | "jeskas") {% () => SubType.JESKA %}
KAITO -> ("kaito" | "kaitos") {% () => SubType.KAITO %}
KARN -> ("karn" | "karns") {% () => SubType.KARN %}
KASMINA -> ("kasmina" | "kasminas") {% () => SubType.KASMINA %}
KAYA -> ("kaya" | "kayas") {% () => SubType.KAYA %}
KIORA -> ("kiora" | "kioras") {% () => SubType.KIORA %}
KOTH -> ("koth" | "koths") {% () => SubType.KOTH %}
LILIANA -> ("liliana" | "lilianas") {% () => SubType.LILIANA %}
LOLTH -> ("lolth" | "lolths") {% () => SubType.LOLTH %}
LUKKA -> ("lukka" | "lukkas") {% () => SubType.LUKKA %}
MINSC -> ("minsc" | "minscs") {% () => SubType.MINSC %}
MORDENKAINEN -> ("mordenkainen" | "mordenkainens") {% () => SubType.MORDENKAINEN %}
NAHIRI -> ("nahiri" | "nahiris") {% () => SubType.NAHIRI %}
NARSET -> ("narset" | "narsets") {% () => SubType.NARSET %}
NIKO -> ("niko" | "nikos") {% () => SubType.NIKO %}
NISSA -> ("nissa" | "nissas") {% () => SubType.NISSA %}
NIXILIS -> "nixilis" {% () => SubType.NIXILIS %}
OKO -> ("oko" | "okos") {% () => SubType.OKO %}
RAL -> ("ral" | "rals") {% () => SubType.RAL %}
ROWAN -> ("rowan" | "rowans") {% () => SubType.ROWAN %}
SAHEELI -> ("saheeli" | "saheelis") {% () => SubType.SAHEELI %}
SAMUT -> ("samut" | "samuts") {% () => SubType.SAMUT %}
SARKHAN -> ("sarkhan" | "sarkhans") {% () => SubType.SARKHAN %}
SERRA -> ("serra" | "serras") {% () => SubType.SERRA %}
SIVITRI -> ("sivitri" | "sivitris") {% () => SubType.SIVITRI %}
SORIN -> ("sorin" | "sorins") {% () => SubType.SORIN %}
SZAT -> ("szat" | "szats") {% () => SubType.SZAT %}
TAMIYO -> ("tamiyo" | "tamiyos") {% () => SubType.TAMIYO %}
TASHA -> ("tasha" | "tashas") {% () => SubType.TASHA %}
TEFERI -> ("teferi" | "teferis") {% () => SubType.TEFERI %}
TEYO -> ("teyo" | "teyos") {% () => SubType.TEYO %}
TEZZERET -> ("tezzeret" | "tezzerets") {% () => SubType.TEZZERET %}
TIBALT -> ("tibalt" | "tibalts") {% () => SubType.TIBALT %}
TYVAR -> ("tyvar" | "tyvars") {% () => SubType.TYVAR %}
UGIN -> ("ugin" | "ugins") {% () => SubType.UGIN %}
URZA -> ("urza" | "urzas") {% () => SubType.URZA %}
VENSER -> ("venser" | "vensers") {% () => SubType.VENSER %}
VIVIEN -> ("vivien" | "viviens") {% () => SubType.VIVIEN %}
VRASKA -> ("vraska" | "vraskas") {% () => SubType.VRASKA %}
WILL -> ("will" | "wills") {% () => SubType.WILL %}
WINDGRACE -> ("windgrace" | "windgraces") {% () => SubType.WINDGRACE %}
WRENN -> ("wrenn" | "wrenns") {% () => SubType.WRENN %}
XENAGOS -> "xenagos" {% () => SubType.XENAGOS %}
YANGGU -> ("yanggu" | "yanggus") {% () => SubType.YANGGU %}
YANLING -> ("yanling" | "yanlings") {% () => SubType.YANLING %}
ZARIEL -> ("zariel" | "zariels") {% () => SubType.ZARIEL %}
ADVENTURE -> ("adventure" | "adventures") {% () => SubType.ADVENTURE %}
ARCANE -> ("arcane" | "arcanes") {% () => SubType.ARCANE %}
LESSON -> ("lesson" | "lessons") {% () => SubType.LESSON %}
TRAP -> ("trap" | "traps") {% () => SubType.TRAP %}

# endregion

# region KEYWORD_TOKENS

DEATHTOUCH -> "deathtouch" {% () => Keyword.DEATHTOUCH %}
DEFENDER -> "defender" {% () => Keyword.DEFENDER %}
DOUBLE_STRIKE -> "double strike" {% () => Keyword.DOUBLE_STRIKE %}
ENCHANT -> "enchant" {% () => Keyword.ENCHANT %}
EQUIP -> "equip" {% () => Keyword.EQUIP %}
FIRST_STRIKE -> "first strike" {% () => Keyword.FIRST_STRIKE %}
FLASH -> "flash" {% () => Keyword.FLASH %}
FLYING -> "flying" {% () => Keyword.FLYING %}
HASTE -> "haste" {% () => Keyword.HASTE %}
HEXPROOF -> "hexproof" {% () => Keyword.HEXPROOF %}
INDESTRUCTIBLE -> "indestructible" {% () => Keyword.INDESTRUCTIBLE %}
INTIMIDATE -> "intimidate" {% () => Keyword.INTIMIDATE %}
LANDWALK -> "walk" {% () => Keyword.LANDWALK %}
LIFELINK -> "lifelink" {% () => Keyword.LIFELINK %}
PROTECTION -> "protection" {% () => Keyword.PROTECTION %}
REACH -> "reach" {% () => Keyword.REACH %}
SHROUD -> "shroud" {% () => Keyword.SHROUD %}
TRAMPLE -> "trample" {% () => Keyword.TRAMPLE %}
VIGILANCE -> "vigilance" {% () => Keyword.VIGILANCE %}
WARD -> "ward" {% () => Keyword.WARD %}
BANDING -> "banding" {% () => Keyword.BANDING %}
RAMPAGE -> "rampage" {% () => Keyword.RAMPAGE %}
CUMULATIVE_UPKEEP -> "cumulative upkeep" {% () => Keyword.CUMULATIVE_UPKEEP %}
FLANKING -> "flanking" {% () => Keyword.FLANKING %}
PHASING -> "phasing" {% () => Keyword.PHASING %}
BUYBACK -> "buyback" {% () => Keyword.BUYBACK %}
SHADOW -> "shadow" {% () => Keyword.SHADOW %}
CYCLING -> "cycling" {% () => Keyword.CYCLING %}
ECHO -> "echo" {% () => Keyword.ECHO %}
HORSEMANSHIP -> "horsemanship" {% () => Keyword.HORSEMANSHIP %}
FADING -> "fading" {% () => Keyword.FADING %}
KICKER -> "kicker" {% () => Keyword.KICKER %}
FLASHBACK -> "flashback" {% () => Keyword.FLASHBACK %}
MADNESS -> "madness" {% () => Keyword.MADNESS %}
FEAR -> "fear" {% () => Keyword.FEAR %}
MORPH -> "morph" {% () => Keyword.MORPH %}
AMPLIFY -> "amplify" {% () => Keyword.AMPLIFY %}
PROVOKE -> "provoke" {% () => Keyword.PROVOKE %}
STORM -> "storm" {% () => Keyword.STORM %}
AFFINITY -> "affinity" {% () => Keyword.AFFINITY %}
ENTWINE -> "entwine" {% () => Keyword.ENTWINE %}
MODULAR -> "modular" {% () => Keyword.MODULAR %}
SUNBURST -> "sunburst" {% () => Keyword.SUNBURST %}
BUSHIDO -> "bushido" {% () => Keyword.BUSHIDO %}
SOULSHIFT -> "soulshift" {% () => Keyword.SOULSHIFT %}
SPLICE -> "splice" {% () => Keyword.SPLICE %}
OFFERING -> "offering" {% () => Keyword.OFFERING %}
NINJUTSU -> "ninjutsu" {% () => Keyword.NINJUTSU %}
EPIC -> "epic" {% () => Keyword.EPIC %}
CONVOKE -> "convoke" {% () => Keyword.CONVOKE %}
DREDGE -> "dredge" {% () => Keyword.DREDGE %}
TRANSMUTE -> "transmute" {% () => Keyword.TRANSMUTE %}
BLOODTHIRST -> "bloodthirst" {% () => Keyword.BLOODTHIRST %}
HAUNT -> "haunt" {% () => Keyword.HAUNT %}
REPLICATE -> "replicate" {% () => Keyword.REPLICATE %}
FORECAST -> "forecast" {% () => Keyword.FORECAST %}
GRAFT -> "graft" {% () => Keyword.GRAFT %}
RECOVER -> "recover" {% () => Keyword.RECOVER %}
RIPPLE -> "ripple" {% () => Keyword.RIPPLE %}
SPLIT_SECOND -> "split second" {% () => Keyword.SPLIT_SECOND %}
SUSPEND -> "suspend" {% () => Keyword.SUSPEND %}
VANISHING -> "vanishing" {% () => Keyword.VANISHING %}
ABSORB -> "absorb" {% () => Keyword.ABSORB %}
AURA_SWAP -> "aura swap" {% () => Keyword.AURA_SWAP %}
DELVE -> "delve" {% () => Keyword.DELVE %}
FORTIFY -> "fortify" {% () => Keyword.FORTIFY %}
FRENZY -> "frenzy" {% () => Keyword.FRENZY %}
GRAVESTORM -> "gravestorm" {% () => Keyword.GRAVESTORM %}
POISONOUS -> "poisonous" {% () => Keyword.POISONOUS %}
TRANSFIGURE -> "transfigure" {% () => Keyword.TRANSFIGURE %}
CHAMPION -> "champion" {% () => Keyword.CHAMPION %}
CHANGELING -> "changeling" {% () => Keyword.CHANGELING %}
EVOKE -> "evoke" {% () => Keyword.EVOKE %}
HIDEAWAY -> "hideaway" {% () => Keyword.HIDEAWAY %}
PROWL -> "prowl" {% () => Keyword.PROWL %}
REINFORCE -> "reinforce" {% () => Keyword.REINFORCE %}
CONSPIRE -> "conspire" {% () => Keyword.CONSPIRE %}
PERSIST -> "persist" {% () => Keyword.PERSIST %}
WITHER -> "wither" {% () => Keyword.WITHER %}
RETRACE -> "retrace" {% () => Keyword.RETRACE %}
DEVOUR -> "devour" {% () => Keyword.DEVOUR %}
EXALTED -> "exalted" {% () => Keyword.EXALTED %}
UNEARTH -> "unearth" {% () => Keyword.UNEARTH %}
CASCADE -> "cascade" {% () => Keyword.CASCADE %}
ANNIHILATOR -> "annihilator" {% () => Keyword.ANNIHILATOR %}
LEVEL_UP -> "level up" {% () => Keyword.LEVEL_UP %}
REBOUND -> "rebound" {% () => Keyword.REBOUND %}
TOTEM_ARMOR -> "totem armor" {% () => Keyword.TOTEM_ARMOR %}
INFECT -> "infect" {% () => Keyword.INFECT %}
BATTLE_CRY -> "battle cry" {% () => Keyword.BATTLE_CRY %}
LIVING_WEAPON -> "living weapon" {% () => Keyword.LIVING_WEAPON %}
UNDYING -> "undying" {% () => Keyword.UNDYING %}
MIRACLE -> "miracle" {% () => Keyword.MIRACLE %}
SOULBOUND -> "soulbound" {% () => Keyword.SOULBOUND %}
OVERLOAD -> "overload" {% () => Keyword.OVERLOAD %}
SCAVENGE -> "scavenge" {% () => Keyword.SCAVENGE %}
UNLEASH -> "unleash" {% () => Keyword.UNLEASH %}
CIPHER -> "cipher" {% () => Keyword.CIPHER %}
EVOLVE -> "evolve" {% () => Keyword.EVOLVE %}
EXTORT -> "extort" {% () => Keyword.EXTORT %}
FUSE -> "fuse" {% () => Keyword.FUSE %}
BESTOW -> "bestow" {% () => Keyword.BESTOW %}
TRIBUTE -> "tribute" {% () => Keyword.TRIBUTE %}
DETHRONE -> "dethrone" {% () => Keyword.DETHRONE %}
HIDDEN_AGENDA -> "hidden agenda" {% () => Keyword.HIDDEN_AGENDA %}
OUTLAST -> "outlast" {% () => Keyword.OUTLAST %}
PROWESS -> "prowess" {% () => Keyword.PROWESS %}
DASH -> "dash" {% () => Keyword.DASH %}
EXPLOIT -> "exploit" {% () => Keyword.EXPLOIT %}
MENACE -> "menace" {% () => Keyword.MENACE %}
RENOWN -> "renown" {% () => Keyword.RENOWN %}
AWAKEN -> "awaken" {% () => Keyword.AWAKEN %}
DEVOID -> "devoid" {% () => Keyword.DEVOID %}
INGEST -> "ingest" {% () => Keyword.INGEST %}
MYRIAD -> "myriad" {% () => Keyword.MYRIAD %}
SURGE -> "surge" {% () => Keyword.SURGE %}
SKULK -> "skulk" {% () => Keyword.SKULK %}
EMERGE -> "emerge" {% () => Keyword.EMERGE %}
ESCALATE -> "escalate" {% () => Keyword.ESCALATE %}
MELEE -> "melee" {% () => Keyword.MELEE %}
CREW -> "crew" {% () => Keyword.CREW %}
FABRICATE -> "fabricate" {% () => Keyword.FABRICATE %}
PARTNER -> "partner" {% () => Keyword.PARTNER %}
UNDAUNTED -> "undaunted" {% () => Keyword.UNDAUNTED %}
IMPROVISE -> "improvise" {% () => Keyword.IMPROVISE %}
AFTERMATH -> "aftermath" {% () => Keyword.AFTERMATH %}
EMBALM -> "embalm" {% () => Keyword.EMBALM %}
ETERNALIZE -> "eternalize" {% () => Keyword.ETERNALIZE %}
AFFLICT -> "afflict" {% () => Keyword.AFFLICT %}
ASCEND -> "ascend" {% () => Keyword.ASCEND %}
ASSIST -> "assist" {% () => Keyword.ASSIST %}
JUMP_START -> "jump start" {% () => Keyword.JUMP_START %}
MENTOR -> "mentor" {% () => Keyword.MENTOR %}
AFTERLIFE -> "afterlife" {% () => Keyword.AFTERLIFE %}
RIOT -> "riot" {% () => Keyword.RIOT %}
SPECTACLE -> "spectacle" {% () => Keyword.SPECTACLE %}
ESCAPE -> "escape" {% () => Keyword.ESCAPE %}
COMPANION -> "companion" {% () => Keyword.COMPANION %}
MUTATE -> "mutate" {% () => Keyword.MUTATE %}
ENCORE -> "encore" {% () => Keyword.ENCORE %}
BOAST -> "boast" {% () => Keyword.BOAST %}
FORETELL -> "foretell" {% () => Keyword.FORETELL %}
DEMONSTRATE -> "demonstrate" {% () => Keyword.DEMONSTRATE %}
DAYBOUND_NIGHTBOUND -> ("daybound" | "nightbound") {% () => Keyword.DAYBOUND_NIGHTBOUND %}
DISTURB -> "disturb" {% () => Keyword.DISTURB %}
DECAYED -> "decayed" {% () => Keyword.DECAYED %}
CLEAVE -> "cleave" {% () => Keyword.CLEAVE %}
TRAINING -> "training" {% () => Keyword.TRAINING %}
COMPLEATED -> "compleated" {% () => Keyword.COMPLEATED %}
RECONFIGURE -> "reconfigure" {% () => Keyword.RECONFIGURE %}
BLITZ -> "blitz" {% () => Keyword.BLITZ %}
CASUALTY -> "casualty" {% () => Keyword.CASUALTY %}
ENLIST -> "enlist" {% () => Keyword.ENLIST %}
READ_AHEAD -> "read ahead" {% () => Keyword.DEFENDER %}
RAVENOUS -> "ravenous" {% () => Keyword.RAVENOUS %}
SQUAD -> "squad" {% () => Keyword.SQUAD %}
SPACE_SCULPTOR -> "space sculptor" {% () => Keyword.SPACE_SCULPTOR %}
VISIT -> "visit" {% () => Keyword.VISIT %}
PROTOTYPE -> "prototype" {% () => Keyword.PROTOTYPE %}
LIVING_METAL -> "living metal" {% () => Keyword.LIVING_METAL %}
FOR_MIRRODIN -> "for mirrodin!" {% () => Keyword.FOR_MIRRODIN %}
TOXIC -> "toxic" {% () => Keyword.TOXIC %}

# endregion