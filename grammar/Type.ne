@include "./Enums.ne"

typeLine ->
  (cardType _):+ {% ([cardTypes]) => ({
    cardTypes: cardTypes.map(([type]) => type),
    superTypes: [],
    subTypes: [],
  }) %}
  | (superType _):+ (cardType _):+ {% ([superTypes, cardTypes]) => ({
    cardTypes: cardTypes.map(([type]) => type),
    superTypes: superTypes.map(([type]) => type),
    subTypes: [],
  }) %}
  | (cardType _):+ "-" __ (subType _):+ {% ([cardTypes, , , subTypes]) => ({
    cardTypes: cardTypes.map(([type]) => type),
    superTypes: [],
    subTypes: subTypes.map(([type]) => type),
  }) %}
  | (superType _):+ (cardType _):+ "-" __ (subType _):+ {% ([superTypes, cardTypes, , , subTypes]) => ({
    cardTypes: cardTypes.map(([type]) => type),
    superTypes: superTypes.map(([type]) => type),
    subTypes: subTypes.map(([type]) => type),
  }) %}